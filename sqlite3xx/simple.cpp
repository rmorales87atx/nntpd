//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"

enum {
	T_INT,
	T_UINT,
	T_INT64,
	T_UINT64,
	T_FLOAT,
	T_DOUBLE,
	T_CHAR,
	T_CHAR16,
	T_WCHAR, // win32 only
	T_INT_OPTIONAL,
	T_UINT_OPTIONAL,
	T_INT64_OPTIONAL,
	T_UINT64_OPTIONAL,
	T_DOUBLE_OPTIONAL,
};

sqlite3xx::detail::statement_temporary sqlite3xx::database::operator<<(std::string_view sql) const
{
	return compile(sql);
}

sqlite3xx::null_column_value::null_column_value(detail::statement_temporary::out_info& dst, statement& stmt)
	: error{ "NULL column value accessed" }
{
	auto iref = *dst.index;
	if (auto pval = std::get_if<int>(&iref)) {
		index_ = *pval;
		refname_ = stmt.field_name(*index_);
	}
	if (auto pval = std::get_if<std::string_view>(&iref)) {
		refname_ = *pval;
		index_ = stmt.field_index(refname_);
	}
	message_ = std::string("NULL value accessed in column \"" + refname_ + "\"");
}

std::optional<int> sqlite3xx::null_column_value::index() const noexcept
{
	return index_;
}

std::string sqlite3xx::null_column_value::name() const noexcept
{
	return refname_;
}

char const* sqlite3xx::null_column_value::what() const noexcept
{
	return message_.c_str();
}

sqlite3xx::detail::statement_temporary::statement_temporary(statement&& st)
	: stmt{ st }
	, no_exec{ false }
	, bind_count_{ 0 }
	, column_index_{ 0 }
{}

sqlite3xx::detail::statement_temporary::~statement_temporary() noexcept(false)
{
	if (!no_exec) {
		while (step())
			;
	}
}

bool sqlite3xx::detail::statement_temporary::step()
{
	if (stmt.step() && !stmt.eof()) {
		column_index_ = 0;
		for (auto& buf : outbuf_) {
			if (!buf.index) {
				buf.index = column_index_;
			}
			auto column = stmt.field_value(*buf.index);
			copy_into(buf, column);
			column_index_++;
		}
		return true;
	}
	return false;
}

void sqlite3xx::detail::statement_temporary::copy_into(out_info& dst, sqlite3xx::column& src)
{
	assert(dst.ptr != nullptr);
	switch (dst.type) {
	case T_INT:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(int));
			auto ref = src.get_int(0);
			memcpy(dst.ptr, &ref, dst.siz);
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_UINT:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(unsigned int));
			auto ref = src.get_uint(0);
			memcpy(dst.ptr, &ref, dst.siz);
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_INT64:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(int64_t));
			auto ref = src.get_int64(0);
			memcpy(dst.ptr, &ref, dst.siz);
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_UINT64:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(uint64_t));
			auto ref = src.get_uint64(0);
			memcpy(dst.ptr, &ref, dst.siz);
			break;
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_FLOAT:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(float));
			auto ref = static_cast<float>(src.get_double(0));
			memcpy(dst.ptr, &ref, dst.siz);
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_DOUBLE:
		if (!src.is_null()) {
			assert(dst.siz == sizeof(double));
			auto ref = src.get_double(0);
			memcpy(dst.ptr, &ref, dst.siz);
		} else {
			throw null_column_value(dst, stmt);
		}
		break;
	case T_CHAR:
		if (dst.siz == sizeof(char)) {
			std::string& out{ *reinterpret_cast<std::string*>(dst.ptr) };
			out = src.get_text();
		}
		break;
	case T_CHAR16:
		if (dst.siz == sizeof(char16_t)) {
			std::u16string& out{ *reinterpret_cast<std::u16string*>(dst.ptr) };
			out = src.get_text16();
		}
		break;
	case T_WCHAR:
#if defined(_MSC_VER) || defined(WIN32)
		if (dst.siz == sizeof(wchar_t)) {
			assert(dst.siz == 2);
			std::wstring& out{ *reinterpret_cast<std::wstring*>(dst.ptr) };
			out = src.get_textW();
		}
		break;
#else
		throw error{ "wstring not available on this platform" };
#endif
	case T_INT_OPTIONAL:
		if (dst.ptr) {
			auto& ref{ *reinterpret_cast<std::optional<int>*>(dst.ptr) };
			ref = src.get_int();
		}
		break;
	case T_UINT_OPTIONAL:
		if (dst.ptr) {
			auto& ref{ *reinterpret_cast<std::optional<unsigned int>*>(dst.ptr) };
			ref = src.get_uint();
		}
		break;
	case T_INT64_OPTIONAL:
		if (dst.ptr) {
			auto& ref{ *reinterpret_cast<std::optional<int64_t>*>(dst.ptr) };
			ref = src.get_int64();
		}
		break;
	case T_UINT64_OPTIONAL:
		if (dst.ptr) {
			auto& ref{ *reinterpret_cast<std::optional<uint64_t>*>(dst.ptr) };
			ref = src.get_uint64();
		}
		break;
	case T_DOUBLE_OPTIONAL:
		if (dst.ptr) {
			auto& ref{ *reinterpret_cast<std::optional<double>*>(dst.ptr) };
			ref = src.get_double();
		}
		break;
	}
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<int> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_int(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<unsigned int> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_int(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<int64_t> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_int64(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<uint64_t> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_uint64(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<float> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_double(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<double> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_double(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::string> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::string_view> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<char const*> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::u16string> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::u16string_view> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<char16_t const*> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

#if defined(_MSC_VER) || defined(WIN32)

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::wstring> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<std::wstring_view> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(use_value<wchar_t const*> uv)
{
	bind_count_++;
	if (!uv.index) uv.index = bind_count_;
	stmt.bind_text(*uv.index, uv.value_);
	return *this;
}

#endif

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<int> iref)
{
	outbuf_.emplace_back(out_info{ T_INT, &iref.ref_, sizeof(int), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<unsigned int> iref)
{
	outbuf_.emplace_back(out_info{ T_UINT, &iref.ref_, sizeof(unsigned int), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<int64_t> iref)
{
	outbuf_.emplace_back(out_info{ T_INT64, &iref.ref_, sizeof(int64_t), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<uint64_t> iref)
{
	outbuf_.emplace_back(out_info{ T_UINT64, &iref.ref_, sizeof(uint64_t), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<float> iref)
{
	outbuf_.emplace_back(out_info{ T_FLOAT, &iref.ref_, sizeof(float), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<double> iref)
{
	outbuf_.emplace_back(out_info{ T_DOUBLE, &iref.ref_, sizeof(double), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<std::string> iref)
{
	outbuf_.emplace_back(out_info{ T_CHAR, &iref.ref_, sizeof(char), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<std::u16string> iref)
{
	outbuf_.emplace_back(out_info{ T_CHAR16, &iref.ref_, sizeof(char16_t), iref.index });
	return *this;
}

#if defined(_MSC_VER) || defined(WIN32)

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_ref<std::wstring> iref)
{
	outbuf_.emplace_back(out_info{ T_WCHAR, &iref.ref_, sizeof(wchar_t), iref.index });
	return *this;
}

#endif

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_opt<int> iref)
{
	outbuf_.emplace_back(out_info{ T_INT_OPTIONAL, &iref.ref_, sizeof(int), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_opt<unsigned int> iref)
{
	outbuf_.emplace_back(out_info{ T_UINT_OPTIONAL, &iref.ref_, sizeof(unsigned int), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_opt<int64_t> iref)
{
	outbuf_.emplace_back(out_info{ T_INT64_OPTIONAL, &iref.ref_, sizeof(int64_t), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_opt<uint64_t> iref)
{
	outbuf_.emplace_back(out_info{ T_UINT64_OPTIONAL, &iref.ref_, sizeof(uint64_t), iref.index });
	return *this;
}

sqlite3xx::detail::statement_temporary& sqlite3xx::detail::statement_temporary::operator,(into_opt<double> iref)
{
	outbuf_.emplace_back(out_info{ T_DOUBLE_OPTIONAL, &iref.ref_, sizeof(double), iref.index });
	return *this;
}

sqlite3xx::rowset::rowset(detail::statement_temporary& src)
	: holder_{ std::move(src) }
{
	src.no_exec = true;
}

sqlite3xx::statement& sqlite3xx::rowset::get_statement()
{
	return holder_.stmt;
}

bool sqlite3xx::rowset::next()
{
	return holder_.step();
}

sqlite3xx::statement* sqlite3xx::rowset::operator->()
{
	return &holder_.stmt;
}
