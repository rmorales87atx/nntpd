﻿//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

using std::optional;
using std::nullopt;
using namespace std::placeholders;

std::string sqlite3xx::escape_text(char fmt_type, std::string_view const what)
{
	std::unique_ptr<char, std::function<void(char*)>> bufptr{ nullptr, [](char* ptr) { sqlite3_free(ptr); } };
	std::string const input{ what };
	switch (fmt_type) {
	case 'q':
		bufptr.reset(sqlite3_mprintf("%q", input.c_str()));
		break;
	case 'Q':
		bufptr.reset(sqlite3_mprintf("%Q", input.c_str()));
		break;
	case 'w':
		bufptr.reset(sqlite3_mprintf("%w", input.c_str()));
		break;
	}
	if (bufptr) {
		return std::string(bufptr.get());
	}
	return std::string();
}

std::string sqlite3xx::escape_text(char fmt_type, column const& value)
{
	return escape_text(fmt_type, value.get_text());
}

void sqlite3xx::initialize()
{
	int res = sqlite3_initialize();
	if (res != SQLITE_OK) throw error{ res, std::nullopt };
	std::atexit(&sqlite3xx::shutdown);
}

void sqlite3xx::shutdown()
{
	sqlite3_shutdown();
}
