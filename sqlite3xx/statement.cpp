//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::statement::statement(database const& db)
	: sql_{ db.handle() }
	, eof_{ true }
	, ncols_{ 0 }
	, db_{ db }
{}

sqlite3xx::statement::statement(statement const& ref)
	: sql_{ ref.sql_ }
	, vmptr_{ ref.vmptr_ }
	, eof_{ ref.eof_ }
	, ncols_{ ref.ncols_ }
	, names_{ ref.names_ }
	, db_{ ref.db_ }
{}

sqlite3xx::statement::statement(statement&& ref)
	: sql_{ ref.sql_ }
	, vmptr_{ std::move(ref.vmptr_) }
	, eof_{ ref.eof_ }
	, ncols_{ ref.ncols_ }
	, names_{ ref.names_ }
	, db_{ ref.db_ }
{}

sqlite3xx::statement& sqlite3xx::statement::operator=(statement&& ref)
{
	sql_ = std::move(ref.sql_);
	vmptr_ = std::move(ref.vmptr_);
	eof_ = ref.eof_;
	ncols_ = ref.ncols_;
	names_ = ref.names_;
	return *this;
}

sqlite3xx::statement::statement(database const& db, std::shared_ptr<sqlite3_stmt> vm)
	: sql_{ db.handle() }
	, vmptr_{ vm }
	, eof_{ false }
	, ncols_{ 0 }
	, db_{ db }
{
	assert(vmptr_.get() != nullptr);
	ncols_ = sqlite3_column_count(vmptr_.get());
	init_names();
}

sqlite3xx::statement::~statement()
{
	try {
		finalize();
	} catch (...) {}
}

sqlite3xx::statement::statement(database const& db, std::string_view sql_text)
	: sql_{ db.handle() }
	, eof_{ false }
	, ncols_{ 0 }
	, db_{ db }
{
	*this = db.compile(sql_text);
}

bool sqlite3xx::statement::is_valid() const
{
	return sql_ != nullptr && vmptr_ != nullptr;
}

int sqlite3xx::statement::changes()
{
	if (sql_) {
		return sqlite3_changes(sql_);
	}
	return 0;
}

int sqlite3xx::statement::param_count() const
{
	return sqlite3_bind_parameter_count(vmptr_.get());
}

std::string sqlite3xx::statement::param_name(int n) const
{
	return sqlite3_bind_parameter_name(vmptr_.get(), n);
}

int sqlite3xx::statement::bind(index_reference index, column const& val) const
{
	switch (val.type()) {
	case SQLITE_INTEGER:
		return bind_int64(index, *val.get_int64());
	case SQLITE_FLOAT:
		return bind_double(index, *val.get_double());
	case SQLITE_BLOB:
		return bind_blob(index, val.get_blob());
	case SQLITE_NULL:
		return bind_null(index);
	case SQLITE3_TEXT:
		return bind_text(index, val.get_text());
	default:
		return SQLITE_MISMATCH;
	}
}

int sqlite3xx::statement::bind_text(index_reference index, std::string_view const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		if (value.empty()) {
			return sqlite3_bind_null(vmptr_.get(), *i);
		} else {
			return sqlite3_bind_text(vmptr_.get(), *i, value.data(), (int)value.size(), SQLITE_TRANSIENT);
		}
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_text(index_reference index, std::u16string_view const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		if (value.empty()) {
			return sqlite3_bind_null(vmptr_.get(), *i);
		} else {
			return sqlite3_bind_text16(vmptr_.get(), *i, value.data(), (int)value.size(), SQLITE_TRANSIENT);
		}
	}
	return SQLITE_ERROR;
}

#if defined(_MSC_VER) || defined(WIN32)

int sqlite3xx::statement::bind_text(index_reference index, std::wstring_view const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		if (value.empty()) {
			return sqlite3_bind_null(vmptr_.get(), *i);
		} else {
			return sqlite3_bind_text16(vmptr_.get(), *i, value.data(), (int)value.size(), SQLITE_TRANSIENT);
		}
	}
	return SQLITE_ERROR;
}

#endif

int sqlite3xx::statement::bind_int(index_reference index, std::optional<int> const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		auto vptr = vmptr_.get();
		return value ? sqlite3_bind_int(vptr, *i, *value) : sqlite3_bind_null(vptr, *i);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_int64(index_reference index, std::optional<int64_t> const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		auto vptr = vmptr_.get();
		return value ? sqlite3_bind_int64(vmptr_.get(), *i, *value) : sqlite3_bind_null(vptr, *i);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_uint64(index_reference index, std::optional<uint64_t> const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		auto vptr = vmptr_.get();
		return value ? sqlite3_bind_int64(vmptr_.get(), *i, *value) : sqlite3_bind_null(vptr, *i);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_double(index_reference index, std::optional<double> const value) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		auto vptr = vmptr_.get();
		return value ? sqlite3_bind_double(vmptr_.get(), *i, *value) : sqlite3_bind_null(vptr, *i);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_blob(index_reference index, std::vector<char> const& buf) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		if (buf.size() != 0) {
			return sqlite3_bind_blob(vmptr_.get(), *i, &buf[0], (int)buf.size(), SQLITE_TRANSIENT);
		} else {
			return sqlite3_bind_null(vmptr_.get(), *i);
		}
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::bind_null(index_reference index) const
{
	auto i = get_param(index);
	if (vmptr_ && i) {
		return sqlite3_bind_null(vmptr_.get(), *i);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::statement::reset()
{
	if (vmptr_) {
		int res = sqlite3_reset(vmptr_.get());
		if (res == SQLITE_OK) {
			eof_ = false;
		}
		return res;
	}
	return SQLITE_ERROR;
}

void sqlite3xx::statement::finalize()
{
	vmptr_.reset();
	ncols_ = 0;
	eof_ = true;
	names_.clear();
}

#if defined(SQLITE_ENABLE_COLUMN_METADATA)

std::string sqlite3xx::statement::database_name(int n) const
{
	if (vmptr_) {
		return sqlite3_column_database_name(vmptr_.get(), n);
	} else {
		return std::string{};
	}
}

std::string sqlite3xx::statement::table_name(int n) const
{
	if (vmptr_) {
		return sqlite3_column_table_name(vmptr_.get(), n);
	} else {
		return std::string{};
	}
}

#endif

int sqlite3xx::statement::field_count() const
{
	return ncols_;
}

std::optional<int> sqlite3xx::statement::field_index(std::string_view what) const
{
	std::string const name{ what };
	auto iter = names_.find(name);
	if (iter != names_.end()) {
		return iter->second;
	} else if (db_.get_error_mask() & invalid_column) {
		throw error{ "invalid column: " + name };
	}
	return std::nullopt;
}

std::string sqlite3xx::statement::field_name(int index) const
{
	if (vmptr_) {
		auto strp = sqlite3_column_name(vmptr_.get(), index);
		if (strp) {
			return std::string{ strp };
		}
	}
	return std::string{};
}

std::optional<int> sqlite3xx::statement::get_column(index_reference& ref) const
{
	if (auto pval = std::get_if<int>(&ref); pval && *pval >= 0) {
		return *pval;
	} else if (auto strval = std::get_if<std::string_view>(&ref)) {
		return field_index(*strval);
	} else if (db_.get_error_mask() & invalid_column) {
		throw error{ "invalid column reference" };
	}
	return std::nullopt;
}

std::optional<int> sqlite3xx::statement::get_param(index_reference& ref) const
{
	if (auto pval = std::get_if<int>(&ref); pval && *pval >= 1) {
		return *pval;
	} else if (auto strval = std::get_if<std::string_view>(&ref)) {
		std::string str{ *strval };
		int n = sqlite3_bind_parameter_index(vmptr_.get(), str.c_str());
		if (n > 0) {
			return n;
		}
	}
	if (db_.get_error_mask() & invalid_column) {
		throw error{ "invalid parameter reference" };
	}
	return std::nullopt;
}

std::string sqlite3xx::statement::field_decltype(index_reference ref) const
{
	if (vmptr_) {
		if (auto i = get_column(ref)) {
			return sqlite3_column_decltype(vmptr_.get(), *i);
		}
	}
	return std::string{};
}

std::optional<int> sqlite3xx::statement::field_data_type(index_reference ref) const
{
	if (vmptr_) {
		if (auto i = get_column(ref)) {
			return sqlite3_column_type(vmptr_.get(), *i);
		}
	}
	return std::nullopt;
}

sqlite3xx::column sqlite3xx::statement::field_value(index_reference ref) const
{
	if (vmptr_) {
		if (auto i = get_column(ref)) {
			return column{ db_, vmptr_, *i };
		}
	}
	return column{ db_ };
}

sqlite3xx::column sqlite3xx::statement::operator()(index_reference ref) const
{
	return field_value(ref);
}

bool sqlite3xx::statement::is_field_null(index_reference ref) const
{
	if (auto dt = field_data_type(ref)) {
		return *dt == SQLITE_NULL;
	} else {
		return true;
	}
}

bool sqlite3xx::statement::step()
{
	if (vmptr_) {
		int res = sqlite3_step(vmptr_.get());
		switch (res) {
		case SQLITE_DONE:
			eof_ = true;
			break;
		case SQLITE_ROW:
			eof_ = false;
			break;
		default:
			db_.raise_error(error_mask::step);
			finalize();
			break;
		}
		return res == SQLITE_DONE || res == SQLITE_ROW;
	}
	return false;
}

bool sqlite3xx::statement::eof() const
{
	return eof_;
}

void sqlite3xx::statement::init_names()
{
	names_.clear();
	if (vmptr_) {
		for (int ix = 0; ix < ncols_; ++ix) {
			std::string name = sqlite3_column_name(vmptr_.get(), ix);
			names_[name] = ix;
		}
	}
}
