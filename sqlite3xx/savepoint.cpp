//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::savepoint::savepoint(database& db, std::string_view name)
	: db_{ db }
	, autocommit_{ true }
	, active_{ false }
{
	start(name);
}

sqlite3xx::savepoint::savepoint(database& db)
	: db_{ db }
	, autocommit_{ true }
	, active_{ false }
{}

sqlite3xx::savepoint::~savepoint()
{
	try {
		rollback();
	} catch (...) {}
}

void sqlite3xx::savepoint::start(std::string_view name)
{
	assert(name.length() > 0);
	if (!active_) {
		autocommit_ = db_.get_autocommit();
		name_ = sqlite3xx::escape_text('w', name);
		db_.exec_dml("SAVEPOINT " + name_);
		active_ = true;
	}
}

bool sqlite3xx::savepoint::is_active() const
{
	return active_;
}

void sqlite3xx::savepoint::release()
{
	if (active_) {
		db_.exec_dml("RELEASE SAVEPOINT " + name_);
		if (autocommit_) db_.exec_dml("COMMIT");
		active_ = false;
	}
}

void sqlite3xx::savepoint::rollback()
{
	if (active_) {
		db_.exec_dml("ROLLBACK TO SAVEPOINT " + name_);
		if (autocommit_) db_.exec_dml("ROLLBACK");
		active_ = false;
	}
}

sqlite3xx::statement sqlite3xx::savepoint::exec_query(std::string_view sql_text)
{
	return db_.exec_query(sql_text);
}

int sqlite3xx::savepoint::exec_dml(std::string_view sql_text)
{
	return db_.exec_dml(sql_text);
}

sqlite3xx::column sqlite3xx::savepoint::exec_scalar(std::string_view sql_text)
{
	return db_.exec_scalar(sql_text);
}
