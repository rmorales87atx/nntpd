//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)

sqlite3xx::session sqlite3xx::database::create_session(std::string_view name)
{
	return sqlite3xx::session{ *this, name };
}

sqlite3xx::session::session(database const& db, std::string_view name_)
	: db_{ db }
{
	std::string const name{ name_ };
	sqlite3_session* obj = nullptr;
	int res = sqlite3session_create(db.handle(), name.c_str(), &obj);
	if (res != SQLITE_OK) {
		db.raise_error(error_mask::session_create);
	}
	objptr_.reset(obj, sqlite3session_delete);
}

sqlite3xx::session::session(session&& other) noexcept
	: db_{ other.db_ }
	, objptr_{ other.objptr_ }
	, filter_{ other.filter_ }
{
}

sqlite3_session* sqlite3xx::session::handle() const
{
	return objptr_.get();
}

int sqlite3xx::session::attach(std::string_view name_)
{
	std::string const name{ name_ };
	int res = sqlite3session_attach(handle(), name.c_str());
	if (res != SQLITE_OK)
		db_.raise_error(error_mask::session_attach);
	return res;
}

bool sqlite3xx::session::is_enabled() const
{
	return 1 == sqlite3session_enable(handle(), -1);
}

int sqlite3xx::session::set_enabled(int state)
{
	int res = sqlite3session_enable(objptr_.get(), state);
	if (res != SQLITE_OK)
		db_.raise_error(error_mask::session_enable);
	return res;
}

bool sqlite3xx::session::empty() const
{
	return 0 != sqlite3session_isempty(handle());
}

bool sqlite3xx::session::get_indirect() const
{
	return 1 == sqlite3session_indirect(handle(), -1);
}

int sqlite3xx::session::set_indirect(bool state)
{
	int res = sqlite3session_indirect(handle(), state ? 1 : 0);
	if (res != SQLITE_OK)
		db_.raise_error(error_mask::session_indirect);
	return res;
}

int sqlite3xx::session::diff(std::string_view from_, std::string_view tab_)
{
	char* pzErrMsg = nullptr;
	std::string const from_db{ from_ }, tab{ tab_ };
	int res = sqlite3session_diff(handle(), from_db.c_str(), tab.c_str(), &pzErrMsg);
	if (res != SQLITE_OK)
		db_.raise_error(error_mask::session_diff);
	return res;
}

void sqlite3xx::session::set_table_filter(std::function<int(session&, std::string_view)> xFilter)
{
	filter_ = xFilter;
	if (filter_) {
		sqlite3session_table_filter(handle(), [](void* pCtx, char const* zTab) -> int {
			auto ssn = reinterpret_cast<session*>(pCtx);
			return ssn->filter_(*ssn, zTab);
		}, this);
	} else {
		sqlite3session_table_filter(objptr_.get(), nullptr, nullptr);
	}

}

sqlite3xx::changeset sqlite3xx::session::changes()
{
	int nbytes = 0;
	void* data = nullptr;
	int res = sqlite3session_changeset(handle(), &nbytes, &data);
	if (res == SQLITE_OK) {
		return changeset{ db_, nbytes, data };
	} else {
		db_.raise_error(error_mask::session_changeset);
		return changeset{ db_, 0, nullptr };
	}
}

sqlite3xx::changeset sqlite3xx::session::patchset()
{
	int nbytes = 0;
	void* data = nullptr;
	int res = sqlite3session_patchset(handle(), &nbytes, &data);
	if (res == SQLITE_OK) {
		return changeset{ db_, nbytes, data };
	} else {
		db_.raise_error(error_mask::session_patchset);
		return changeset{ db_, 0, nullptr };
	}
}

#endif
