//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::value::value(std::shared_ptr<sqlite3_value> vptr)
	: valptr_{ vptr }
{}

sqlite3xx::value::value(sqlite3_value* vptr)
	: valptr_{ vptr, [](sqlite3_value*) {} }
{}

sqlite3xx::value::value()
	: valptr_{}
{}

sqlite3xx::value::value(value const& other)
{
	valptr_.reset(sqlite3_value_dup(other.valptr_.get()), [](sqlite3_value* ptr) { sqlite3_value_free(ptr); });
}

sqlite3xx::value::value(value&& fv) noexcept
	: valptr_{ std::move(fv.valptr_) }
{}

int sqlite3xx::value::type() const
{
	if (valptr_) {
		return sqlite3_value_type(valptr_.get());
	} else {
		return SQLITE_NULL;
	}
}

bool sqlite3xx::value::is_null() const
{
	return type() == SQLITE_NULL;
}

int sqlite3xx::value::numeric_type() const
{
	if (valptr_) {
		return sqlite3_value_numeric_type(valptr_.get());
	} else {
		return SQLITE_ERROR;
	}
}

std::optional<int> sqlite3xx::value::get_int() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_value_int(valptr_.get());
	}
}

std::optional<int64_t> sqlite3xx::value::get_int64() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_value_int64(valptr_.get());
	}
}

std::optional<unsigned int> sqlite3xx::value::get_uint() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return (unsigned int)sqlite3_value_int(valptr_.get());
	}
}

std::optional<uint64_t> sqlite3xx::value::get_uint64() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return (uint64_t)sqlite3_value_int64(valptr_.get());
	}
}

std::optional<double> sqlite3xx::value::get_double() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_value_double(valptr_.get());
	}
}

int sqlite3xx::value::get_int(int def) const
{
	return is_null() ? def : *get_int();
}

int64_t sqlite3xx::value::get_int64(int64_t def) const
{
	return is_null() ? def : *get_int64();
}

unsigned int sqlite3xx::value::get_uint(unsigned int def) const
{
	return is_null() ? def : *get_uint();
}

uint64_t sqlite3xx::value::get_uint64(uint64_t def) const
{
	return is_null() ? def : *get_uint64();
}

double sqlite3xx::value::get_double(double def) const
{
	return is_null() ? def : *get_double();
}

std::string sqlite3xx::value::get_text() const
{
	if (is_null()) {
		return {};
	} else {
		return { c_str() };
	}
}

std::u16string sqlite3xx::value::get_text16() const
{
	if (is_null()) {
		return {};
	} else {
		return { c_str16() };
	}
}

char const* sqlite3xx::value::c_str() const
{
	if (is_null()) {
		return nullptr;
	} else {
		return reinterpret_cast<char const*>(sqlite3_value_text(valptr_.get()));
	}
}

char16_t const* sqlite3xx::value::c_str16() const
{
	if (is_null()) {
		return nullptr;
	} else {
		return reinterpret_cast<char16_t const*>(sqlite3_value_text16(valptr_.get()));
	}
}

std::vector<char> sqlite3xx::value::get_blob() const
{
	std::vector<char> buf;
	if (!is_null()) {
		size_t nbytes = sqlite3_value_bytes(valptr_.get());
		if (nbytes > 0) {
			buf.resize(nbytes);
			memcpy(&buf[0], reinterpret_cast<char const*>(sqlite3_value_blob(valptr_.get())), nbytes);
		}
	}
	return buf;
}

std::ostream& sqlite3xx::value::operator<<(std::ostream& os) const
{
	if (!is_null()) {
		os << reinterpret_cast<char const*>(sqlite3_value_text(valptr_.get()));
	}
	return os;
}
