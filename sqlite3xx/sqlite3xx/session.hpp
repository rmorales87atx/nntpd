//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

#if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)

namespace sqlite3xx {
	/// SQLite session changeset
	class changeset {
	public:
		/// Changeset iterator
		class iterator {
			friend class changeset;
		public:
			// Default constructor
			iterator(database const&, sqlite3_changeset_iter*);

			// Copy constructor
			iterator(iterator const&) = delete;

			// Move constructor
			iterator(iterator&&) noexcept;

			// Return table name
			std::string table() const;

			// Return number of columns
			int ncols() const;

			// Return the change type
			int type() const;

			// Determine if a change was "indirect"
			int get_indirect() const;

			// Return the new value for the specified column
			value new_value(int nCol) const;

			// Return the old value for the specified column
			value old_value(int nCol) const;

			// Retrieve the next change
			int next();

			// Determine if there are any more changes
			bool eof() const;

		private:
			database const& db_;
			sqlite3* sql_;
			std::shared_ptr<sqlite3_changeset_iter> iter_;
			bool eof_;
			std::string ztab_;
			int ncol_, op_, indirect_;
			void get_data();
		};

		// Default changeset constructor
		changeset(database const&, int, void*);

		// No copy constructor
		changeset(changeset const&) = delete;

		// Move constructor
		changeset(changeset&&) noexcept;

		// Default destructor
		virtual ~changeset();

		// Return iterator for the first item in the changeset
		iterator start() const;

		// Set-up filter callback for apply()
		void set_apply_filter(std::function<int(void* pCtx, std::string_view)> xFilter);

		// Set-up conflict callback for apply()
		void set_apply_conflict(std::function<int(void* pCtx, int eConflict, iterator const& op)> xConflict);

		// Attempt to apply the changeset
		int apply(void* pCtx = nullptr);

		// Concatenate another changeset object with this one
		int concat(changeset&);

		// Produce an inverted changeset
		changeset invert() const;

		// Retrieve the number of raw data bytes for the changeset
		int size() const;

		// Retrieve the raw change data
		void* data() const;

		// Assume ownership of a buffer, presumed to be changeset data
		// The buffer must have been allocated with sqlite3_malloc()
		// as it will be deallocated with sqlite3_free().
		void assign(int nbytes, void* buf);

	private:
		database const& db_;
		int size_;
		void* data_;
		sqlite3* sql_;
		struct context_data {
			void* user_ptr;
			changeset& owner;
		};
		std::function<int(void* pCtx, std::string_view strTab)> m_xFilter;
		std::function<int(void* pCtx, int eConflict, iterator const& op)> m_xConflict;
	};

	/// SQLite session monitor object
	class session {
	public:
		// Default constructor
		session(database const&, std::string_view);

		// Move constructor
		session(session&&) noexcept;

		// Raw SQLite object pointer
		sqlite3_session* handle() const;

		// Attach session to the specified table, or all tables if not specified.
		int attach(std::string_view name = "");

		// Retrieve a changeset.
		changeset changes();

		// Retrieve a patchset.
		changeset patchset();

		// Determine if this session object is enabled.
		bool is_enabled() const;

		// Change the session object's enabled status.
		int set_enabled(int);

		// Determine if the session has an empty changeset
		bool empty() const;

		// Determine if the indirect flag is set.
		bool get_indirect() const;

		// Change the session object's indirect flag.
		int set_indirect(bool);

		// Determine the difference between tables
		int diff(std::string_view from_db, std::string_view tab);

		// Set table filter
		void set_table_filter(std::function<int(session&, std::string_view)> xFilter);

	private:
		database const& db_;
		std::shared_ptr<sqlite3_session> objptr_;
		std::function<int(session&, std::string_view)> filter_;
	};
}

#endif //SQLITE_ENABLE_SESSION && SQLITE_ENABLE_PREUPDATE_HOOK
