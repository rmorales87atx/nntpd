﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace nntpd {
	// Forward declarations
	class client;

	/// NNTP session flags
	enum session_flags : unsigned int {
		SF_QUIT           = 00001, // User has QUIT and is disconnected
		SF_ADMIN          = 00002, // User has an admin account
		SF_AUTH           = 00004, // User has logged in
		SF_SECURE         = 00010, // User connected via TLS
		SF_MULTILINE      = 00020, // Receiving multi-line data 
		SF_NEWSGROUP      = 00040, // Selected newsgroup
		SF_GOT_AUTHINFO   = 00100, // Received AUTHINFO USER, waiting for AUTHINFO PASS
	};

	using client_ptr = std::shared_ptr<client>;
	using multiline_handler = std::function<void(client_ptr src, size_t nlines, std::string_view data)>;

	/// Class representing an active NNTP client session.
	class client: public std::enable_shared_from_this<client> {
	public:
		unsigned int flags;
		std::string newsgroup;
		std::optional<int64_t> cur_article_id;
		std::string account; // Received from AUTHUSER INFO
		std::string tls_fp; // TLS fingerprint

		client(socket_ptr, server&);

		virtual ~client();

		std::string endpoint_id() const;

		/// Activate TLS
		void enable_tls();

		// Close connection
		// If a message is provided, it will transmit as response code 400
		void kill(std::string const& message);

		// Process data from connection
		void read();

		// Returns the sptr's known IP address
		std::string address() const;

		// Send data
		void write(std::stringstream const& str, bool send_dot = true);

		// Send numeric reply with multi-line data
		void send(response_code rc, std::stringstream const& sstr);

		// Send numeric reply
		void reply(response_code rc);

		// Send numeric reply
		template<typename First, typename... Args>
		void reply(response_code rc, First&& f, Args&&... args)
		{
			write(generate_reply(rc, f, args...));
		}

		// Establish multi-line receiving mode
		bool set_multiline(multiline_handler completion_handler);

	private:
		server& server_;
		socket_ptr socket_;
		std::string endpt_id_;
		std::unique_ptr<asio::ssl::stream<tcp::socket&>> ssl_socket_;
		std::list<std::string> queue_;
		asio::streambuf data_;
		time_t last_action_;
		std::vector<char> multiline_buf_;
		size_t multiline_count_;
		multiline_handler multiline_done_;
		void close();
		void send_message();
		void process(std::string const& line);
		void read_handler(boost::system::error_code const& err, std::size_t nbytes);
		void write(std::string const& str);
		bool ssl_verify_certificate(bool preverified, asio::ssl::verify_context& ctx);
		void ssl_handshake(boost::system::error_code const& err);
	};
}
