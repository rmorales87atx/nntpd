﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

inline bool nntpd::text_comparator_less::operator()(std::string_view lhs, std::string_view rhs) const
{
	return compare_text(lhs, rhs) < 0;
}

inline bool nntpd::text_comparator::operator()(std::string_view lhs, std::string_view rhs) const
{
	return compare_text(lhs, rhs) == 0;
}

template <typename iterator>
std::wstring nntpd::str_widen(iterator start, iterator end)
{
	std::wstring out;
#if BOOST_OS_WINDOWS
	static_assert(sizeof(wchar_t) == 2);
	utf8::utf8to16(start, end, std::back_inserter(out));
#else
	static_assert(sizeof(wchar_t) == 4);
	utf8::utf8to32(start, end, std::back_inserter(out));
#endif
	return out;
}

template <typename iterator>
std::string nntpd::str_narrow(iterator start, iterator end)
{
	std::string out;
#if BOOST_OS_WINDOWS
	static_assert(sizeof(wchar_t) == 2);
	utf8::utf16to8(start, end, std::back_inserter(out));
#else
	static_assert(sizeof(wchar_t) == 4);
	utf8::utf32to8(start, end, std::back_inserter(out));
#endif
	return out;
}

inline std::wstring nntpd::str_widen(std::string_view str)
{
	return str_widen(std::begin(str), std::end(str));
}

inline std::string nntpd::str_narrow(std::wstring_view str)
{
	return str_narrow(std::begin(str), std::end(str));
}

template<typename First, typename... Args>
std::string nntpd::generate_reply(response_code rc, First&& first, Args&&... args)
{
	using namespace boost::io;
	std::string format = generate_reply(rc);
	if (format.length() <= 3) {
		std::stringstream ss;
		ss << format << " " << first;
		boost::format f{ ss.str() };
		f.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(void)(f % ... % std::forward<Args>(args));
		return f.str();
	} else {
		boost::format f{ format };
		f.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		f% first;
		(void)(f % ... % std::forward<Args>(args));
		return f.str();
	}
}

inline auto nntpd::get_crlf_lines(std::string const& text)
{
	return boost::tokenizer{ text, boost::char_separator<char>{ "\r\n", nullptr, boost::drop_empty_tokens } };
}

template<typename T>
inline auto nntpd::get_crlf_lines(T&& begin, T&& end)
{
	return boost::tokenizer(begin, end, boost::char_separator<char>{ "\r\n", nullptr, boost::drop_empty_tokens });
}
