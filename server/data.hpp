﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace nntpd {
	constexpr char const* RFC2822_DATE_FORMAT = "%a, %d %b %Y %H:%M:%S %z";

	// Forward declarations
	class server;
	class client;
	struct config_info;

	using socket_ptr = std::shared_ptr<tcp::socket>;

	/// Configurable options
	enum service_options : unsigned int {
		NNTP_ENABLE_TLS           = 0000001,
		NNTP_REQUIRE_AUTH_POST    = 0000002,
		NNTP_REQUIRE_TLS_AUTH     = 0000004,
		NNTP_XLISTGROUPS          = 0000010,
		NNTP_8BIT_TO_BASE64       = 0000020,
		NNTP_CONVERT_UTF8_HEADERS = 0000040,
		NNTP_SECURE_DELETE        = 0000100,
	};

	/// Data loaded from configuration file
	struct config_info {
		std::string db_file;
		std::optional<int> idle_timeout;
		std::string site_name;
		std::map<std::string, int> listen;
		std::string tls_private_key;
		std::string tls_cert_file;
		std::map<std::string, int> tls_bind;
		unsigned int options;
		int win32_view_limit;
		int win32_view_init;
		std::set<std::string, text_comparator_less> mime_types;
	};

	/// Action codes used for server::allow_group()
	enum allow_group_action {
		GROUP_READ = 1,
		GROUP_POST,
		GROUP_MODERATE,
	};

	/// Data from parse_article()
	struct post_info {
		std::unordered_map<std::string, std::string> headers;
		std::string body;
		std::string references;
		std::string group_name;
		size_t lines;
		size_t bytes;
	};

	/// Group information
	struct group_info {
		std::string name;
		std::string title;
		std::string description;
		int auto_expire;
		bool restrict_read;
		bool restrict_post;
		bool require_tls;
		size_t body_limit;
		size_t subject_limit;
		int64_t count;
		int64_t high_id;
		int64_t low_id;
	};

	/// Article information
	struct article_info {
		std::string group;
		int64_t article_id;
		std::string message_id;
		std::string references;
		std::string subject;
		std::string from;
		std::string content_type;
		std::string encoding;
		int64_t date;
		int64_t expire;
		size_t bytes;
		size_t lines;
		ordered_map_text<std::string, std::string> headers;
	};

	/// Flags used for header processing
	enum header_flags : unsigned int {
		HF_ACCEPT       = 00001, // Accept from client posts
		HF_REQUIRED     = 00002, // Header must be present in POST
		HF_REJECT       = 00004, // Reject article if this header is present
		HF_MSGID        = 00010, // Header refers to an article message-id
		HF_GROUP        = 00020, // Header refers to a single newsgroup
		HF_CTL_OPTIONAL = 00040, // Header is not required in control messages
		HF_SKIP_TBL     = 00100, // Header not processed using the table in some functions (see get_headers)
	};

	/// Article header information
	struct header_info {
		std::string rfc_name;
		unsigned int flags;
	};

	/// Flags returned by server::get_group_access()
	enum group_access_flags : unsigned int {
		GA_READ      = 001,
		GA_POST      = 002,
		GA_MODERATOR = 004,
	};

	/// Load the XML configuration file
	std::optional<config_info> load_config_xml(std::string source);
}
