﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using boost::system::error_code;
using namespace std::placeholders;

namespace nntpd {
	static std::map<std::string, unsigned int> const conf_options = {
		{ "require_auth_post", NNTP_REQUIRE_AUTH_POST },
		{ "require_tls_auth", NNTP_REQUIRE_TLS_AUTH },
		{ "xlistgroups", NNTP_XLISTGROUPS },
		{ "8bit_to_base64", NNTP_8BIT_TO_BASE64 },
		{ "convert_utf8_headers", NNTP_CONVERT_UTF8_HEADERS },
		{ "secure_delete", NNTP_SECURE_DELETE },
	};

	static unsigned int const default_options
		= NNTP_REQUIRE_AUTH_POST
		| NNTP_XLISTGROUPS
		| NNTP_8BIT_TO_BASE64
		| NNTP_CONVERT_UTF8_HEADERS;
}

std::optional<nntpd::config_info> nntpd::load_config_xml(std::string source)
{
	using boost::lexical_cast;
	if (source.empty()) {
		source = "config.xml";
	}
	config_info result;
	result.options = default_options;
	result.win32_view_init = 0;
	result.win32_view_limit = 100;
	pugi::xml_document doc;
	if (!doc.load_file(source.c_str())) {
		return std::nullopt;
	}
	auto root = doc.child("nntpd");
	if (auto section = root.child("server")) {
		result.db_file = section.child("database").text().as_string("news.db");
		if (auto value = section.child("idle_timeout")) {
			result.idle_timeout = value.text().as_int();
		}
		result.site_name = section.child("site_name").text().as_string();
		if (!strscan(result.site_name, R"(^[\w\.]+$)")) {
			throw std::runtime_error{ "invalid site_name" };
		}
		for (auto&& key : section.children("bind")) {
			std::string addr = key.attribute("address").as_string();
			result.listen[addr] = key.attribute("port").as_int();
		}
	}
	if (auto section = root.child("tls")) {
		result.tls_private_key = section.child("private_key_file").text().as_string();
		result.tls_cert_file = section.child("certificate_chain_file").text().as_string();
		for (auto&& key : section.children("bind")) {
			std::string addr = key.attribute("address").as_string();
			result.tls_bind[addr] = key.attribute("port").as_int();
		}
		if (!result.tls_private_key.empty() && !result.tls_bind.empty()) {
			result.options |= NNTP_ENABLE_TLS;
		} else {
			result.options &= ~NNTP_ENABLE_TLS;
		}
	}
	for (auto&& key : root.children("options")) {
		std::string opt_name = key.attribute("id").as_string();
		if (!conf_options.count(opt_name)) continue;
		auto opt_flag = conf_options.at(opt_name);
		int key_value = key.attribute("value").as_int();
		if (key_value != 0) {
			result.options |= opt_flag;
		} else {
			result.options &= ~opt_flag;
		}
	}
	if (auto section = root.child("win32")) {
		result.win32_view_init = section.child("view_init").text().as_int();
		result.win32_view_limit = section.child("view_limit").text().as_int();
	}
	return std::make_optional(result);
}

void nntpd::server::load_mime_types()
{
	assert(config.has_value());
	config->mime_types.clear();
	pugi::xml_document doc;
	if (doc.load_file("mime-types.xml")) {
		for (auto&& key : doc.child("types").children()) {
			config->mime_types.insert(key.child_value());
		}
	}
	for (char const* type : { "multipart/mixed", "text/plain" }) {
		if (0 == config->mime_types.count(type)) {
			config->mime_types.insert(type);
		}
	}
}
