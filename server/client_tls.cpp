﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using boost::system::error_code;
using namespace std::placeholders;

void nntpd::client::enable_tls()
{
	if (!ssl_socket_ && socket_ && socket_->is_open()) {
		ssl_socket_ = std::make_unique<asio::ssl::stream<tcp::socket&>>(*socket_, server_.ssl_context);
		ssl_socket_->set_verify_mode(asio::ssl::verify_peer);
		ssl_socket_->set_verify_callback(std::bind(&client::ssl_verify_certificate, this, _1, _2));
		ssl_socket_->async_handshake(asio::ssl::stream_base::server, std::bind(&client::ssl_handshake, this, _1));
	}
}

bool nntpd::client::ssl_verify_certificate(bool preverified, asio::ssl::verify_context& ctx)
{
	int depth = X509_STORE_CTX_get_error_depth(ctx.native_handle());
	if (depth == 0) {
		unsigned cb;
		unsigned char md[EVP_MAX_MD_SIZE];
		std::stringstream ss;
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		auto rc = X509_digest(cert, EVP_sha256(), md, &cb);
		for (unsigned i = 0; i < cb; i++) {
			ss << std::setw(2) << std::setfill('0') << std::hex << (int)(md[i]);
		}
		tls_fp = ss.str();
	}
	return true;
}

void nntpd::client::ssl_handshake(boost::system::error_code const& err)
{
	if (err) {
		server_.write_log(shared_from_this(), "Error establishing secure connection: %1%", err.message());
		ssl_socket_.reset();
		kill("");
	} else {
		flags |= SF_SECURE;
		reply(RC_STATUS_NO_POST);
		if (tls_fp.empty()) {
			server_.write_log(shared_from_this(), "Secure connection established (no certificate)");
		} else {
			server_.write_log(shared_from_this(), "Secure connection established: %1%", tls_fp);
		}
		asio::post(server_.ios, std::bind(&client::read, this));
	}
}
