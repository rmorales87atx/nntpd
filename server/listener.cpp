﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using boost::system::error_code;
using namespace std::placeholders;

nntpd::listener::listener(nntpd::server& srv, tcp::endpoint endpoint, bool secure)
	: server_{ srv }
	, ios_{ srv.ios }
	, acceptor_{ ios_ }
	, use_tls_{ secure }
{
	acceptor_.open(endpoint.protocol());
	acceptor_.set_option(tcp::acceptor::reuse_address(true));
	acceptor_.bind(endpoint);
	acceptor_.listen();
	process();
}

nntpd::listener::~listener()
{
	error_code ec;
	acceptor_.cancel(ec);
	acceptor_.close(ec);
}

void nntpd::listener::process()
{
	auto sockptr = std::make_shared<tcp::socket>(ios_);
	acceptor_.async_accept(*sockptr, std::bind(&listener::handle, this, _1, sockptr));
}

void nntpd::listener::handle(boost::system::error_code const& err, socket_ptr sptr)
{
	if (!err) {
		// process incoming connection
		auto conn = server_.new_client(sptr);
		auto endpt = sptr->local_endpoint();
		server_.write_log(conn, "New connection on %1%:%2%", endpt.address().to_string(), endpt.port());
		if (use_tls_) {
			conn->enable_tls();
		} else {
			if (server_.has_option(NNTP_REQUIRE_AUTH_POST)) {
				conn->reply(RC_STATUS_NO_POST);
			} else {
				conn->reply(RC_STATUS_POSTING_OK);
			}
			conn->read();
		}
		// continue processing connections
		process();
	}
}
