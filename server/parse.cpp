﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>

static
std::string& encode64(std::string& val, std::optional<size_t> break_ = std::nullopt)
{
	using namespace boost::archive::iterators;
	using ConvIter = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
	auto tmp = std::string{ ConvIter{ std::begin(val) }, ConvIter{ std::end(val) } };
	val = tmp.append((3 - val.size() % 3) % 3, '=');
	if (break_) {
		size_t const n = *break_;
		for (size_t i = n; i < val.size(); i += n + 2) {
			val.insert(i, "\r\n");
		}
	}
	return val;
}

static
bool is_utf8_multibyte(std::string_view string)
{
	if (string.empty())
		return false;

	int num = 0, max_bytes = 0;

	for (auto byte = std::begin(string); byte != std::end(string);) {
		if ((*byte & 0x80) == 0x00) {
			// U+0000 to U+007F 
			num = 1;
		} else if ((*byte & 0xE0) == 0xC0) {
			// U+0080 to U+07FF 
			num = 2;
		} else if ((*byte & 0xF0) == 0xE0) {
			// U+0800 to U+FFFF 
			num = 3;
		} else if ((*byte & 0xF8) == 0xF0) {
			// U+10000 to U+10FFFF 
			num = 4;
		} else {
			return false;
		}
		if (num > max_bytes) max_bytes = num;
		std::advance(byte, 1);
		for (int i = 1; i < num; ++i) {
			if ((*byte & 0xC0) != 0x80)
				return false;
			std::advance(byte, 1);
		}
	}

	return max_bytes > 1;
}

static
bool is_ascii_text(std::string_view string)
{
	for (auto i = std::begin(string); i != std::end(string); i++) {
		if (!isascii(*i)) {
			return false;
		}
	}
	return true;
}

static
inline void check_mime_type(nntpd::server& sptr, std::string const& type_value)
{
	char base_type[1024] = { 0 };
	if (0 == sscanf(type_value.c_str(), "%1023[^;]%*[^\r\n]", base_type)) {
		throw nntpd::article_rejected{ "invalid Content-Type" };
	}
	if (nntpd::same_text(base_type, "text/plain") && !nntpd::strscan(type_value, R"(^\s*text\/plain\s*;\s*charset=\s*(?:utf-8|us-ascii).*$)")) {
		throw nntpd::article_rejected{ "Content-Type \"text/plain\" must have charset utf-8 or us-ascii" };
	}
	if (0 == sptr.config->mime_types.count(base_type)) {
		throw nntpd::article_rejected{ "Content-Type not allowed: " + std::string(base_type) };
	}
}

void nntpd::server::parse_article(std::string_view data, post_info& res, nntpd::client_ptr src)
{
	static boost::regex hdr_line_rex{ R"(^(?:([\w\-]+):\s+|[ \t]\s*)([^\r\n]*)[\r\n]*$)" };
	res.headers.clear();
	res.body.clear();
	res.body.reserve(data.size());
	std::string last_header;
	res.bytes = 0;
	auto i = std::begin(data);
	auto end = std::end(data);
	auto start = std::begin(data);
	if (auto find = data.find("\r\n\r\n"); find != std::string::npos) {
		i = std::begin(data) + find + 4;
		res.body.assign(i, end);
		res.bytes += res.body.size();
	}
	bool got_lines = (res.lines != 0);
	for (auto&& line : get_crlf_lines(start, i)) {
		if (got_lines && res.lines > 0) res.lines--;
		boost::smatch mat;
		if (boost::regex_match(line, mat, hdr_line_rex)) {
			std::string name{ mat[1].first, mat[1].second };
			std::string value{ mat[2].first, mat[2].second };
			if (is_utf8_multibyte(value) && has_option(NNTP_CONVERT_UTF8_HEADERS)) {
				std::string conv = encode64(value);
				value = "=?utf-8?B?" + conv + "?=";
			} else if (!is_ascii_text(value)) {
				throw article_rejected{ "header values must contain only ASCII" };
			}
			if (name.empty()) {
				if (!last_header.empty()) res.headers[last_header].append("\n" + value);
			} else {
				if (same_text(name, "Content-Type")) check_mime_type(*this, value);
				res.headers.insert_or_assign(name, value);
				last_header = name;
			}
		}
		res.bytes += line.size() + 2;
	}
	if (got_lines && res.lines > 0) res.lines--; // empty line between header & body
	bool is_ctrl_msg = (0 == res.headers.count("Control"));
	// article must have headers
	if (0 == res.headers.size()) {
		throw article_rejected{ "invalid format" };
	}
	// validate headers
	for (auto&& [name, info] : header_fields) {
		auto iter = res.headers.find(info.rfc_name);
		if ((info.flags & HF_REQUIRED) && iter == std::end(res.headers) && (!is_ctrl_msg || (info.flags & HF_CTL_OPTIONAL))) {
			throw article_rejected{ "missing required header \"" + info.rfc_name + "\"" };
		}
		if ((info.flags & HF_ACCEPT) && iter == std::end(res.headers)) {
			continue;
		} else if ((info.flags & HF_REJECT) && iter != std::end(res.headers)) {
			throw article_rejected{ "header \"" + info.rfc_name + "\" is not allowed" };
		} else if ((info.flags & (HF_ACCEPT | HF_MSGID)) == (HF_ACCEPT | HF_MSGID)) {
			auto val = db.exec_scalar("SELECT count(*) FROM [articles] WHERE [message_id]=?", iter->second);
			if (0 == val.get_int(0)) {
				throw article_rejected{ "header \"" + info.rfc_name + "\" has invalid message-id" };
			}
		} else if ((info.flags & (HF_ACCEPT | HF_GROUP)) == (HF_ACCEPT | HF_GROUP)) {
			auto val = db.exec_scalar("SELECT count(*) FROM [groups] WHERE [name]=?", iter->second);
			if (0 == val.get_int(0)) {
				throw article_rejected{ "header \"" + info.rfc_name + "\" has invalid newsgroup" };
			}
		}
	}
	// if posted with "Supersedes", ensure it's processed as a control message
	if (res.headers.count("Supersedes")) {
		if (res.headers.count("Control")) {
			throw article_rejected{ "can't use Supersedes and Control at the same time" };
		}
		res.headers["Control"] = "CANCEL " + res.headers.at("Supersedes");
	}
	// article must have a body, if it's not a control message
	if (0 == res.lines && !is_ctrl_msg) {
		throw article_rejected{ "missing body" };
	}
	// verify that poster can access newsgroup
	auto grp = get_group(res.headers["Newsgroups"]);
	if (!grp) {
		throw article_rejected{ "missing newsgroup" };
	}
	if (!allow_group(src, grp->name, GROUP_POST) && 0 == res.headers.count("Control")) {
		throw article_rejected{ "not authorized to post in " + grp->name };
	}
	res.group_name = grp->name;
	// "Content-Transfer-Encoding" defaults to "7bit" if unspecified
	if (!res.headers.count("Content-Transfer-Encoding")) {
		res.headers["Content-Transfer-Encoding"] = "7bit";
	}
	std::string const& encoding{ res.headers.at("Content-Transfer-Encoding") };
	// "Content-Transfer-Encoding" must be only one of: 7bit, 8bit, or base64
	if (!same_text(encoding, "8bit") && !same_text(encoding, "7bit") && !same_text(encoding, "base64")) {
		if (has_option(NNTP_8BIT_TO_BASE64)) {
			throw article_rejected{ "Content-Transfer-Encoding must be 7bit, 8bit, or base64" };
		} else {
			throw article_rejected{ "Content-Transfer-Encoding must be 7bit or base64" };
		}
	}
	// 8bit encoding has some special handling
	if (same_text(encoding, "8bit")) {
		// 8bit is only allowed if the option is enabled
		if (has_option(NNTP_8BIT_TO_BASE64)) {
			encode64(res.body, 76);
			res.headers["Content-Transfer-Encoding"] = "base64";
		} else {
			throw article_rejected{ "Content-Transfer-Encoding must be 7bit or base64" };
		}
	}
	// limit length of the subject line
	if (res.headers["Subject"].length() > grp->subject_limit) {
		throw article_rejected{ format_str("subject too long, cannot exceed %1% bytes", grp->subject_limit) };
	}
	// limit size of the body text
	if (res.body.size() > grp->body_limit) {
		throw article_rejected{ format_str("body exceeds %1% bytes", grp->body_limit) };
	}
	// validate "References"
	// multiple references are space/CRLF delimited - the 'last' reference is the "reply to"
	if (res.headers.count("References") != 0) {
		std::stringstream ss;
		ss << res.headers["References"];
		while (ss >> res.references) {
			auto val = db.exec_scalar("SELECT count(*) FROM [articles] WHERE [group]=? AND [message_id]=?", res.group_name, res.references);
			if (0 == val.get_int(0)) {
				throw article_rejected{ "invalid reference to article " + res.references };
			}
		}
	}
	// always generate the "From" heading based on current login
	if (src->flags & SF_AUTH) {
		auto info = db.exec_query("SELECT * FROM [accounts] WHERE [email]=?", src->account);
		res.headers["From"] = format_str("\"%1%\" <%2%>", info("display_name"), info("email"));
	}
	// change tab characters to spaces in the subject line
	boost::replace_all(res.headers["Subject"], "\t", " ");
}
