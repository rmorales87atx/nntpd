﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"
#include "bcrypt.hpp"

using namespace std::placeholders;

#define COMMAND(EXPR,HANDLER) { EXPR, std::bind(&nntpd::server::HANDLER, this, _1, _2, _3) }

void nntpd::server::init_commands()
{
	cmd_handlers_ = {
		COMMAND("DATE", do_date),
		COMMAND("HELP", do_help),
		COMMAND("QUIT", do_quit),
		COMMAND("MODE", do_mode),
		COMMAND("CAPABILITIES", do_capab),
		COMMAND("LIST", do_list),
		COMMAND("LISTGROUP", do_group),
		COMMAND("GROUP", do_group),
		COMMAND("OVER", do_over),
		COMMAND("XOVER", do_over),
		COMMAND("ARTICLE", do_article),
		COMMAND("LAST", do_last),
		COMMAND("NEXT", do_next),
		COMMAND("STAT", do_stat),
		COMMAND("HEAD", do_head),
		COMMAND("BODY", do_body),
		COMMAND("HDR", do_hdr),
		COMMAND("XHDR", do_hdr),
		COMMAND("POST", do_post),
		COMMAND("AUTHINFO", do_authinfo),
		COMMAND("XLISTGROUPS", do_xlistgroups),
		COMMAND("NEWGROUPS", do_newdata),
		COMMAND("NEWNEWS", do_newdata),
	};
}

void nntpd::server::command(client_ptr sptr, std::string const& data)
{
	std::vector<std::string> args;
	if (strscan(data, R"(^\s*(\S+)(?:\s+([^\r\n]+))?\s*$)", args)) {
		auto i = cmd_handlers_.find(args[1]);
		if (i != std::end(cmd_handlers_)) {
			std::string data;
			if (args.size() > 2) data = args[2];
			i->second(sptr, args[1], data);
			return;
		}
	}
	sptr->reply(RC_INVALID_COMMAND);
	write_log(sptr, "Unknown command: %1%", data);
}

void nntpd::server::do_capab(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	static std::pair<char const*, unsigned int> const caps[] = {
		{ "VERSION 2", 0 },
		{ "IMPLEMENTATION rfc3977", 0 },
		{ "READER", 0 },
		{ "HDR", 0 },
		{ "LIST ACTIVE NEWSGROUPS OVERVIEW.FMT HEADERS", 0 },
		{ "POST", 0 },
		{ "OVER MSGID", 0 },
		{ "AUTHINFO USER", 0 },
		{ "XLISTGROUPS", NNTP_XLISTGROUPS },
	};
	if (data.empty()) {
		std::stringstream out;
		for (auto&& [text, opt] : caps) {
			if (opt == 0 || has_option(opt)) {
				out << text << "\r\n";
			}
		}
		sptr->send(RC_CAPABILITIES, out);
	} else {
		sptr->reply(RC_SYNTAX_ERROR);
	}
}

void nntpd::server::do_quit(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	sptr->reply(RC_QUIT);
	sptr->flags = SF_QUIT;
	// kill connection & destroy client object
	// (but don't do this right now incase we're inside an asio callback)
	asio::post(ios, [sptr]() { sptr->kill(""); });
}

void nntpd::server::do_mode(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (same_text(data, "READER")) {
		sptr->newsgroup.clear();
		sptr->cur_article_id = std::nullopt;
		sptr->flags &= ~SF_NEWSGROUP;
		if (has_option(NNTP_REQUIRE_AUTH_POST) && (sptr->flags & SF_AUTH)) {
			sptr->reply(RC_STATUS_POSTING_OK);
		} else {
			sptr->reply(RC_STATUS_NO_POST);
		}
	} else {
		sptr->reply(RC_SYNTAX_ERROR);
	}
}

void nntpd::server::do_date(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	sptr->reply(RC_DATE, nntpd::timestamp("%Y%m%d%H%M%S"));
}

void nntpd::server::do_help(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	std::stringstream out;
	out << "100 Basic Internet News Service\r\n";
	out << "Version 1.0.0\r\n";
	for (auto&& [name, hnd] : cmd_handlers_) {
		out << name << "\r\n";
	}
	sptr->write(out);
}

void nntpd::server::do_list(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (data.empty() || same_text(data, "ACTIVE")) {
		std::stringstream out;
		for (auto const& group : get_groups()) {
			bool can_post = allow_group(sptr, group.name, GROUP_POST);
			out << group.name << " "
				<< group.high_id << " "
				<< group.low_id << " "
				<< (can_post ? "y" : "n")
				<< "\r\n";
		}
		sptr->send(RC_LISTGROUP, out);
	} else if (same_text(data, "NEWSGROUPS")) {
		std::stringstream out;
		for (auto const& group : get_groups()) {
			out << group.name << " "
				<< group.title
				<< "\r\n";
		}
		sptr->send(RC_LISTGROUP, out);
	} else if (same_text(data, "OVERVIEW.FMT")) {
		std::stringstream out;
		out << "215 List overview format" << "\r\n"
			<< "Subject:" << "\r\n"
			<< "From:" << "\r\n"
			<< "Date:" << "\r\n"
			<< "Message-ID:" << "\r\n"
			<< "References:" << "\r\n"
			<< ":bytes" << "\r\n"
			<< ":lines" << "\r\n";
		sptr->write(out);
	} else if (same_text(data, "HEADERS")) {
		std::stringstream out;
		out << "215 Article headers\r\n";
		for (auto&& [db_name, info] : header_fields) {
			out << info.rfc_name << "\r\n";
		}
		out << ":bytes" << "\r\n";
		out << ":lines" << "\r\n";
		sptr->write(out);
	}
}

void nntpd::server::do_group(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	std::vector<std::string> args;
	if (!strscan(data, R"(^([\w\.\-]+)?$)", args)) {
		sptr->reply(RC_SYNTAX_ERROR);
		return;
	}
	std::string target{ args[1] };
	if (target.empty()) {
		// allow LISTGROUP to use 'current newsgroup'
		if (same_text(command, "LISTGROUP") && (sptr->flags & SF_NEWSGROUP)) {
			target = sptr->newsgroup;
		} else {
			sptr->reply(RC_UNKNOWN_GROUP);
			return;
		}
	}
	if (!allow_group(sptr, target, GROUP_READ)) {
		write_log(sptr, "Denied access to read group %1%", target);
		if (!(sptr->flags & SF_AUTH)) {
			sptr->reply(RC_MUST_AUTH);
		} else {
			sptr->reply(RC_NOT_ALLOWED);
		}
	} else if (auto grp = get_group(target)) {
		sptr->newsgroup = grp->name;
		sptr->flags |= SF_NEWSGROUP;
		if (grp->count == 0) {
			sptr->cur_article_id = std::nullopt;
		} else {
			sptr->cur_article_id = grp->low_id;
		}
		sptr->reply(RC_GROUP, grp->count, grp->low_id, grp->high_id, grp->name);
		if (same_text(command, "LISTGROUP")) {
			// send article IDs
			std::stringstream out;
			for (auto&& info : group_articles(grp->name)) {
				out << info.article_id << "\r\n";
			}
			sptr->write(out);
		}
	} else {
		sptr->reply(RC_UNKNOWN_GROUP);
	}
}

void nntpd::server::do_over(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (!(sptr->flags & SF_NEWSGROUP)) {
		sptr->reply(RC_NO_SELECTED_GROUP);
		return;
	}
	std::stringstream out;
	for (auto&& info : get_articles(sptr, data, RC_OVER)) {
		out << info.article_id << '\t'
			<< info.subject << '\t'
			<< info.from << '\t'
			<< nntpd::timestamp(RFC2822_DATE_FORMAT, info.date) << '\t'
			<< info.message_id << '\t'
			<< info.references << '\t'
			<< info.bytes << '\t'
			<< info.lines
			<< "\r\n";
	}
	sptr->write(out);
}

void nntpd::server::do_last(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (!(sptr->flags & SF_NEWSGROUP)) {
		sptr->reply(RC_NO_SELECTED_GROUP);
	} else if (!sptr->cur_article_id) {
		sptr->reply(RC_NO_SELECTED_ARTICLE);
	} else if (auto info = last_article(sptr->newsgroup, *sptr->cur_article_id)) {
		sptr->cur_article_id = info->article_id;
		sptr->reply(RC_STAT, info->article_id, info->message_id);
	} else {
		sptr->reply(RC_NO_LAST_ARTICLE);
	}
}

void nntpd::server::do_next(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (!(sptr->flags & SF_NEWSGROUP)) {
		sptr->reply(RC_NO_SELECTED_GROUP);
	} else if (!sptr->cur_article_id) {
		sptr->reply(RC_NO_SELECTED_ARTICLE);
	} else if (auto info = next_article(sptr->newsgroup, *sptr->cur_article_id)) {
		sptr->cur_article_id = info->article_id;
		sptr->reply(RC_STAT, info->article_id, info->message_id);
	} else {
		sptr->reply(RC_NO_NEXT_ARTICLE);
	}
}

void nntpd::server::do_stat(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	auto res = get_articles(sptr, data, RC_STAT);
	if (!res.empty()) {
		// still explicitly set cur_article_id
		// even though get_articles() might have done it anyway
		// (retrieving by Message-ID does not set cur_article_id)
		sptr->cur_article_id = res[0].article_id;
	}
}

void nntpd::server::do_head(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	auto res = get_articles(sptr, data, RC_HEAD);
	if (!res.empty()) {
		std::stringstream out;
		get_headers(res[0], out);
		sptr->write(out);
	}
}

void nntpd::server::do_body(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	auto res = get_articles(sptr, data, RC_BODY);
	if (!res.empty()) {
		std::stringstream out;
		out << get_article_body(res[0].message_id);
		sptr->write(out);
	}
}

void nntpd::server::do_hdr(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	std::vector<std::string> args;
	if (!strscan(data, R"(^(\S+)(?:\s+(\S+))?$)", args)) {
		sptr->reply(RC_SYNTAX_ERROR);
		return;
	}
	auto head = std::find_if(std::begin(header_fields), std::end(header_fields),
		[find = args[1]](auto i) { return same_text(i.second.rfc_name, find); });
	if (head == std::end(header_fields)) {
		sptr->reply(RC_SYNTAX_ERROR);
		return;
	}
	auto records = get_articles(sptr, args[2], RC_HDR);
	if (!records.empty()) {
		std::stringstream out;
		for (auto&& info : records) {
			std::string value;
			if (same_text(args[1], ":lines")) {
				value = std::to_string(info.lines);
			} else if (same_text(args[1], ":bytes")) {
				value = std::to_string(info.bytes);
			} else if (same_text(head->first, "message_id")) {
				value = info.message_id;
			} else if (same_text(head->first, "references")) {
				value = info.references;
			} else if (same_text(head->first, "date")) {
				value = nntpd::timestamp(RFC2822_DATE_FORMAT, info.date);
			} else if (same_text(head->first, "expire")) {
				value = nntpd::timestamp(RFC2822_DATE_FORMAT, info.expire);
			} else if (same_text(head->first, "from")) {
				value = info.from;
			} else if (same_text(head->first, "subject")) {
				value = info.subject;
			} else if (auto i = info.headers.find(head->first); i != std::end(info.headers)) {
				value = i->second;
			}
			boost::replace_all(value, "\r", "");
			boost::replace_all(value, "\n", " ");
			out << info.article_id << " " << value << "\r\n";
		}
		sptr->write(out);
	}
}

void nntpd::server::do_article(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	auto res = get_articles(sptr, data, RC_ARTICLE);
	if (!res.empty()) {
		std::stringstream out;
		get_headers(res[0], out);
		out << "\r\n";
		out << get_article_body(res[0].message_id);
		sptr->write(out);
	}
}

void nntpd::server::do_post(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	if (sptr->flags & SF_AUTH) {
		if (sptr->set_multiline(std::bind(&server::post_article, this, _1, _2, _3))) {
			sptr->reply(RC_POST_CONTINUE);
		} else {
			sptr->reply(RC_NOT_ALLOWED);
		}
	} else {
		sptr->reply(RC_POST_DENIED);
	}
}

void nntpd::server::do_authinfo(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	std::vector<std::string> args;
	if (!strscan(data, R"((USER|PASS)\s+(\S+))", args)) {
		sptr->reply(RC_SYNTAX_ERROR);
	} else if (sptr->flags & SF_AUTH) {
		sptr->reply(RC_NOT_ALLOWED);
	} else if (same_text(args[1], "PASS") && !(sptr->flags & SF_GOT_AUTHINFO)) {
		sptr->reply(RC_AUTHINFO_INVALID);
		asio::post(ios, [sptr] { sptr->kill(""); });
	} else if (has_option(NNTP_REQUIRE_TLS_AUTH) && !(sptr->flags & SF_SECURE)) {
		sptr->reply(RC_TLS_REQUIRED);
	} else if (same_text(args[1], "USER")) {
		int count = db.exec_scalar("SELECT count(*) FROM [accounts] WHERE [email]=? AND [active]<>0", args[2]).get_int(0);
		if (count != 0) {
			sptr->flags |= SF_GOT_AUTHINFO;
			sptr->account = args[2];
			sptr->reply(RC_AUTHINFO_SEND_PASSWORD);
		} else {
			sptr->reply(RC_AUTHINFO_FAILED);
		}
	} else if (same_text(args[1], "PASS")) {
		std::string encoded = db.exec_scalar("SELECT [pw_hash] FROM [accounts] WHERE [email]=?", sptr->account).get_text();
		if (bcrypt::compare(encoded, args[2]) == 0) {
			sptr->flags |= SF_AUTH;
			if (0 != db.exec_scalar("SELECT [admin] FROM [accounts] WHERE [email]=?", sptr->account).get_int(0))
				sptr->flags |= SF_ADMIN;
			sptr->reply(RC_AUTHINFO_ACCEPTED);
			write_log(sptr, "Logged accepted: %1%", sptr->account);
		} else {
			sptr->reply(RC_AUTHINFO_FAILED);
		}
	}
}

void nntpd::server::do_xlistgroups(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	struct xml_string_writer : pugi::xml_writer {
		std::string result;
		virtual void write(const void* data, size_t size)
		{
			result.append(static_cast<const char*>(data), size);
		}
	};
	if (has_option(NNTP_XLISTGROUPS)) {
		pugi::xml_document doc;
		auto root = doc.append_child("groups");
		for (auto q = db.exec_query("SELECT * FROM [group_list] ORDER BY [name]"); !q.eof(); q.step()) {
			auto row = root.append_child("group");
			for (int fx = 0; fx < q.field_count(); fx++) {
				auto fname = q.field_name(fx);
				auto fval = q.field_value(fx).get_text();
				auto elem = row.append_child(fname.c_str());
				if (!fval.empty()) {
					elem.text().set(fval.c_str());
				}
			}
		}
		xml_string_writer xml;
		doc.save(xml, " ", pugi::format_indent, pugi::encoding_utf8);
		boost::replace_all(xml.result, "\n", "\r\n");
		size_t lines = std::count_if(std::begin(xml.result), std::end(xml.result), [](char ch) { return ch == '\n'; });
		std::stringstream out;
		out << "290 " << lines << " " << xml.result.size() << " Group information file" << "\r\n";
		out << xml.result;
		sptr->write(out);
	} else {
		sptr->reply(RC_NOT_ALLOWED);
	}
}

void nntpd::server::do_newdata(nntpd::client_ptr sptr, std::string const& command, std::string const& data)
{
	std::vector<std::string> args;
	if (strscan(data, R"(^(?:([\w\.\-\*\?\,]+)\s+)?(\d{4}|\d{2})(\d{2})(\d{2})\s+(\d{2})(\d{2})(\d{2})(?:\s+(GMT))?$)", args)) {
		std::string& yy{ args[2] };
		std::string& mm{ args[3] };
		std::string& dd{ args[4] };
		std::string& hh{ args[5] };
		std::string& nn{ args[6] };
		std::string& ss{ args[7] };
		bool is_gmt{ args.size() > 7 && same_text(args[8], "GMT") };
		if (yy.length() == 2) {
			/* per RFC 3977, 7.3.1:
			 *
			 * If the first two digits of the year are not specified
			 * (this is supported only for backward compatibility), the year is to
			 * be taken from the current century if yy is smaller than or equal to
			 * the current year, and the previous century otherwise.
			 */
			auto year = date::year_month_day{ date::floor<date::days>(std::chrono::system_clock::now()) }.year();
			int ccval = static_cast<int>(year) / 100; // current century
			int yyval{ date::year{ boost::lexical_cast<int>(yy) } }; // YY value requested
			int yynow = static_cast<int>(year) - (ccval * 100); // YY of current year
			if (yyval > yynow) {
				ccval--;
			}
			yy = std::to_string((ccval * 100) + yyval);
		}
		// TODO: handle is_gmt==false
		// compose ISO date string for SQL query
		std::stringstream sstr;
		sstr << yy << "-" << mm << "-" << dd << " "
			<< hh << ":" << nn << ":" << ss;
		std::string date_str{ sstr.str() };
		// process request
		sstr.str("");
		if (same_text(command, "NEWGROUPS")) {
			auto sql = db.exec_query("SELECT * FROM [group_list] WHERE [created] >= ? ORDER BY [name]", date_str);
			for (; !sql.eof(); sql.step()) {
				bool can_post = allow_group(sptr, sql("name").get_text(), GROUP_POST);
				sstr << sql("name") << " "
					<< sql("high_id") << " "
					<< sql("low_id") << " "
					<< (can_post ? "y" : "n")
					<< "\r\n";
			}
			sptr->reply(RC_NEWGROUPS, date_str);
			sptr->write(sstr);
		} else if (same_text(command, "NEWNEWS")) {
			boost::char_separator<char> const sep(",");
			std::vector<std::string> match;
			for (auto&& token : boost::tokenizer(args[1], sep)) {
				match.push_back("[group] GLOB " + sqlite3xx::escape_text('Q', token));
			}
			if (!match.empty()) {
				std::string q_text{ "SELECT [message_id] FROM [articles] WHERE [date] >= ? AND ((" + join(match, ") OR (") + ")) ORDER BY [group], [article_id]" };
				auto sql = db.exec_query(q_text, date_str);
				for (; !sql.eof(); sql.step()) {
					sstr << sql("message_id") << "\r\n";
				}
				sptr->reply(RC_NEWNEWS, date_str);
				sptr->write(sstr);
			} else {
				sptr->reply(RC_SYNTAX_ERROR);
			}
		}
	} else {
		sptr->reply(RC_SYNTAX_ERROR);
	}
}
