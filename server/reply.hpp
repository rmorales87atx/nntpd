﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace nntpd {
	/// NNTP response codes
	enum response_code : unsigned int {
		RC_HELP = 100,
		RC_CAPABILITIES = 101,
		RC_DATE = 111,
		RC_STATUS_POSTING_OK = 200,
		RC_STATUS_NO_POST = 201,
		RC_QUIT = 205,
		RC_GROUP = 211,
		RC_LISTGROUP = 215,
		RC_ARTICLE = 220,
		RC_HEAD = 221,
		RC_BODY = 222,
		RC_STAT = 223,
		RC_OVER = 224,
		RC_HDR = 225,
		RC_POST_OK = 240,
		RC_NEWNEWS = 230,
		RC_NEWGROUPS = 231,
		RC_POST_CONTINUE = 340,
		RC_ERROR = 400,
		RC_GENERAL_ERROR = 403,
		RC_UNKNOWN_GROUP = 411,
		RC_NO_SELECTED_GROUP = 412,
		RC_NO_SELECTED_ARTICLE = 420,
		RC_NO_NEXT_ARTICLE = 421,
		RC_NO_LAST_ARTICLE = 422,
		RC_ARTICLE_NOT_FOUND = 423,
		RC_MSGID_NOT_FOUND = 430,
		RC_POST_DENIED = 440,
		RC_POST_REJECTED = 441,
		RC_MUST_AUTH = 480,
		RC_TLS_REQUIRED = 483,
		RC_INVALID_COMMAND = 500,
		RC_SYNTAX_ERROR = 501,
		RC_NOT_ALLOWED = 502,

		// Replies used for AUTHINFO (RFC 4643)

		RC_AUTHINFO_ACCEPTED = 281,
		RC_AUTHINFO_SEND_PASSWORD = 381,
		RC_AUTHINFO_FAILED = 481,
		RC_AUTHINFO_INVALID = 482,
	};

	/// Generate reply text from the given response code
	std::string generate_reply(response_code rc);

	/**
	 * @brief Generate formatted reply text
	 *
	 * The response code's text is determined from
	 * the table in <numeric.cpp>
	 *
	 * If an entry from that table is not available:
	 * the parameter <first> is used as a format
	 * specification to format the remaining arguments.
	 */
	template<typename First, typename... Args>
	std::string generate_reply(response_code rc, First&& first, Args&&... args);
}
