﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

#if BOOST_OS_WINDOWS
# include "win32.hpp"
# pragma comment (lib, "crypt32")
# pragma comment (lib, "bcrypt")

using boost::system::error_code;

static CAppModule _Module;
static std::unique_ptr<nntpd::server> sptr;

static
void WinMessagePump()
{
	/* we let boost::asio::io_context act as the primary
	 * event processor, so Win32 messages need to be
	 * manually pumped through this function
	 */
	if (sptr) {
		MSG Msg;
		if (PeekMessageW(&Msg, NULL, 0, 0, 1) > 0) {
			TranslateMessage(&Msg);
			DispatchMessageW(&Msg);
		}
		// reinsert into the io_context
		asio::post(sptr->ios, WinMessagePump);
	}
}

int CALLBACK wWinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow
)
{
	AtlInitCommonControls(ICC_STANDARD_CLASSES);
//	LoadLibrary(CRichEditCtrl::GetLibraryName());
	_Module.Init(nullptr, hInstance);
	std::unique_ptr<CMainForm> form;
	try {
		sptr.reset(new nntpd::server);
		form.reset(new CMainForm{ *sptr });
		form->Create(nullptr);
		form->ShowWindow(SW_NORMAL);
		WinMessagePump();
		sptr->run();
	} catch (sqlite3xx::error const& err) {
		auto text = nntpd::str_widen(err.what());
		MessageBoxW(nullptr, text.c_str(), L"SQLite Error", MB_ICONEXCLAMATION);
	} catch (std::exception const& e) {
		auto text = nntpd::str_widen(e.what());
		MessageBoxW(nullptr, text.c_str(), L"Application Error", MB_ICONEXCLAMATION);
	}
	if (form) form->DestroyWindow();
	form.reset();
	_Module.Term();
	return 0;
}

#elif BOOST_OS_UNIX
# include <sys/time.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>

static std::unique_ptr<nntpd::server> sptr;

static void write_pid()
{
	FILE* fd = nullptr;
	if ((fd = fopen("nntpd.pid", "w"))) {
		fprintf(fd, "%d\n", (int)getpid());
		fclose(fd);
	}
}

static void daemonize()
{
	pid_t pid;

	/* Fork off the parent process */
	if ((pid = fork()) < 0) {
		perror(__FUNCTION__);
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* On success: The child process becomes session leader */
	if (setsid() < 0) {
		perror(__FUNCTION__);
		exit(EXIT_FAILURE);
	}

	/* Catch, ignore and handle signals */
	//TODO: Implement a working signal handler */
	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	/* Fork off for the second time (to prevent zombies) */
	if ((pid = fork()) < 0) {
		perror(__FUNCTION__);
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Set new file permissions */
	umask(0);

	/* Close all open file descriptors */
	for (int x = sysconf(_SC_OPEN_MAX); x >= 0; x--) {
		close(x);
	}

	/* Output PID to file */
	write_pid();
}

int main()
{
	try {
		daemonize();
		sptr.reset(new nntpd::server());
		sptr->run();
	} catch (std::exception const& e) {
		if (sptr) sptr->write_log(e.what());
	}
	return 0;
}

#endif
