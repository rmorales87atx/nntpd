﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace nntpd {
	/// Simple object which opens a TCP listening socket for the given endpoint
	class listener {
	public:
		listener(server& srv, tcp::endpoint endpoint, bool secure);
		~listener();
	private:
		server& server_;
		asio::io_context& ios_;
		tcp::acceptor acceptor_;
		bool const use_tls_;
		void process();
		void handle(boost::system::error_code const& err, socket_ptr sptr);
	};

	/// Exception thrown by server::parse_article()
	class article_rejected : public std::runtime_error {
	public:
		article_rejected(char const* reason) : std::runtime_error{ reason } {}
		article_rejected(std::string&& reason) : std::runtime_error{ reason }{}
	};

	/// NNTP server instance
	class server {
	public:
		asio::io_context ios;
		asio::ssl::context ssl_context;
		sqlite3xx::database db;
		std::optional<config_info> config;
		static ordered_map_text<std::string, header_info> const header_fields;

		server();

		~server();

		/// Begin running the server
		void run();

		/// Terminate server
		void close();

		/// Establish client session object
		client_ptr new_client(socket_ptr sock);

		/// Delete client session
		void remove_client(std::string const& id);

		/// Determine if configuration option is enabled
		bool has_option(unsigned int value) const;

		/// Create TCP listener
		void add_listen(std::string const& host, int port, bool secure);

		/// Write to server log
		int64_t write_log(client_ptr source, std::string const& message);

		/// Write to server log
		int64_t write_log(std::string const& message);

		/// Write to server log
		template<typename... Args>
		inline int64_t write_log(client_ptr source, char const* format, Args&&... args)
		{
			return write_log(source, format_str(format, args...));
		}

		/// Write to server log
		template<typename... Args>
		inline int64_t write_log(char const* format, Args&&... args)
		{
			return write_log(format_str(format, args...));
		}

		/// Process client commands
		void command(client_ptr source, std::string const& data);

		/// Retrieve information about a newsgroup
		std::optional<group_info> get_group(std::string const& group_name);

		/// Retrieve a list of newsgroups
		std::vector<group_info> get_groups();

		/// Retrieve a list of articles for a particular group
		std::vector<article_info> get_articles(std::string const& group);

		/// Retrieve article prior to the given current article
		std::optional<article_info> last_article(std::string const& group, int64_t current_id);

		/// Retrieve article following the given current article
		std::optional<article_info> next_article(std::string const& group, int64_t current_id);

		/// Determine number of articles in a newsgroup
		int64_t count_articles(std::string const& group);

		/// Determine if the given sptr is allowed to use the specified group.
		bool allow_group(client_ptr usr, std::string const& group, allow_group_action action);

		/// Retrieve group access data for the given account
		unsigned int get_group_access(std::string const& account, std::string const& group);

		/**
		 * @brief Retrieve articles by either message-id, article number,
		 *        or the client's current article selector.
		 *
		 * @param usr Client executing the retrieval command
		 * @param param Original parameter from the command handler
		 * @param rc Response code if the article is found
		 * @return A list of requested articles, if any are found
		 *
		 * @remarks
		 * This method is typically called by command handlers to perform
		 * the "grunt work" of understanding the various formats these
		 * commands can accept for article retrieval. (OVER, ARTICLE, etc.)
		 *
		 * If the provided parameter is empty, one article will be retrieved
		 * using the client's cur_article_id session value. Otherwise,
		 * the given parameter's value will be used. This becomes the
		 * "search parameter".
		 *
		 * If the search parameter is a properly-formatted UUID, one article
		 * will be retrieved by message-id. Properly formatted UUIDs must be
		 * enclosed in angle brackets and remain in lower-case.
		 *
		 * If the search parameter is not a valid UUID, the client must have
		 * previously selected a newsgroup via the GROUP command.
		 * This does not apply if the search parameter is a valid UUID.
		 *
		 * If the search parameter is a numeric range, 'n-n' (or 'n-'),
		 * multiple articles may be retrieved. (This is for OVER command.)
		 *
		 * If records were found, get_articles() will generate a response
		 * to the client if parameter [rc] is present. Otherwise,
		 * the appropriate error response will generate.
		 */
		std::vector<article_info> get_articles(nntpd::client_ptr usr, std::string const& param, std::optional<response_code> rc);

		/// Retrieve an article by message-ID.
		std::optional<article_info> get_article(std::string const& msg_id);

		/// Retrieve all articles in the given newsgroup
		std::vector<article_info> group_articles(std::string const& group);

		/// Retrieve article body.
		std::string get_article_body(std::string const& msg_id);

		/**
		 * @brief Generate article header data.
		 *
		 * This method is used to generate article header data for the
		 * HEAD and ARTICLE commands.
		 *
		 * The article data provided in parameter [res] must have been
		 * previously retrieved from the database.
		 */
		void get_headers(article_info const& src, std::stringstream& out);

		/// Generate a new article Message-ID
		std::string generate_msgid();

		/// Parse data received for article posting
		void parse_article(std::string_view data, post_info& result, nntpd::client_ptr src);

		/**
		 * @brief Post an article transmitted by the sptr.
		 *
		 * Prior to using this method, the POST command handler will have set
		 * the client's multi-line receiving mode via sptr::set_multiline().
		 * This method is used as the completion handler to set_multiline(),
		 * which will process and post the presented article.
		 *
		 * Headers are processed, however only a few are retained as part of
		 * the database record. If a "Control" header is present, the article
		 * won't post to the database but will be processed as a "control message".
		 */
		void post_article(nntpd::client_ptr usr, size_t nlines, std::string_view data);

		/// Store article into the database
		std::string store_article(post_info const& data);

		/// Process a "control message" received via POST
		void process_control(nntpd::client_ptr sptr, post_info& data);

		/// Move article to a different group
		int64_t move_article(std::string const& message_id, std::string const& newgroup);
		
	private:
		std::map<std::string, client_ptr> clients_;
		asio::signal_set signals_;
		std::list<std::unique_ptr<listener>> listen_;
		std::unique_ptr<asio::deadline_timer> timer1_;
		using cmd_handler = std::function<void(nntpd::client_ptr sptr, std::string const& command, std::string const& data)>;
		std::map<std::string, cmd_handler, text_comparator_less> cmd_handlers_;
		using ctl_handler = std::function<int(nntpd::client_ptr sptr, std::string const& command, post_info& data)>;
		std::map<std::string, ctl_handler, text_comparator_less> ctl_handlers_;
		void handle_signal(boost::system::error_code const& err, int signal);
		void init_db_conn();
		void init_config();
		void init_commands();
		void init_ctl_handlers();
		void init_timer1();
		void purge_db();
		void load_mime_types();

		/// Command handlers

		void do_quit(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_mode(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_capab(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_date(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_help(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_list(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_group(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_over(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_last(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_next(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_stat(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_head(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_body(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_hdr(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_article(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_post(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_authinfo(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_xlistgroups(nntpd::client_ptr sptr, std::string const& command, std::string const& data);
		void do_newdata(nntpd::client_ptr sptr, std::string const& command, std::string const& data);

		// Control message handlers

		int ctl_group(nntpd::client_ptr sptr, std::string const& command, post_info& data);
		int ctl_rmgroup(nntpd::client_ptr sptr, std::string const& command, post_info& data);
		int ctl_cancel(nntpd::client_ptr sptr, std::string const& command, post_info& data);
	};
}
