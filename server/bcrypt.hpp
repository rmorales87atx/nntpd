﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace bcrypt {
	/// @brief Generate a bcrypt hash with the given key and salt.
	std::string generate_hash(std::string const& key, std::string const& salt);

	/// @brief Generate a bcrypt hash using a randomly-generated salt.
	std::string generate_hash(std::string const& key);

	/// @brief Generate a salt suitable for bcrypt.
	/// On Windows the salt is generated using CryptGenRandom().
	/// On UNIX the salt is generated from /dev/urandom.
	/// @param workload The "work load" value. (Google for details)
	/// @param prefix The prefix value to use. Recommended to leave at default "$2a$".
	std::string generate_salt(unsigned long workload = 12, std::string const& prefix = "$2a$");

	/// @brief Compare a bcrypt encoded value against the specified plaintext.
	int compare(std::string_view encoded, std::string_view plaintext);
}
