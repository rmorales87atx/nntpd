﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using namespace std::placeholders;

#define COMMAND(EXPR,HANDLER) { EXPR, std::bind(&nntpd::server::HANDLER, this, _1, _2, _3) }

void nntpd::server::init_ctl_handlers()
{
	ctl_handlers_ = {
		COMMAND("GROUP", ctl_group),
		COMMAND("RMGROUP", ctl_rmgroup),
		COMMAND("CANCEL", ctl_cancel),
	};
}

void nntpd::server::process_control(nntpd::client_ptr sptr, post_info& data)
{
	try {
		std::vector<std::string> args;
		if (strscan(data.headers.at("Control"), R"(^\s*(\S+)(?:\s+([^\r\n]+))?\s*$)", args)) {
			auto i = ctl_handlers_.find(args[1]);
			if (i != std::end(ctl_handlers_)) {
				if (0 != i->second(sptr, args[0], data)) return;
			}
		}
		sptr->reply(RC_POST_REJECTED, "Invalid request");
	} catch (sqlite3xx::error const& err) {
		auto ref = write_log(sptr, "SQLite error %1%: %2%", err.code().value_or(-1), err.what());
		sptr->kill(format_str("Database error (ref#%1%)", ref));
	} catch (std::exception const& err) {
		auto ref = write_log(sptr, "Internal error: %1%", err.what());
		sptr->kill(format_str("Internal error (ref#%1%)", ref));
	}
}

int nntpd::server::ctl_group(nntpd::client_ptr sptr, std::string const& command, post_info& data)
{
	std::vector<std::string> args;
	if (strscan(command, R"(^GROUP\s+([\w\-\.]+)$)", args) && (sptr->flags & SF_ADMIN)) {
		pugi::xml_document doc;
		if (strmatch(args[1], "ctl.*")) {
			sptr->reply(RC_POST_REJECTED, "Invalid group name");
		} else if (doc.load_buffer(&data.body[0], data.body.size())) {
			auto root = doc.first_child(); // don't really care what the xml root is called
			sqlite3xx::savepoint sp{ db, "group_ctl" };
			if (!get_group(args[1])) {
				sp.exec_dml("INSERT INTO [groups]([name],[title]) VALUES(?,?)", args[1], root.child("title").text().as_string());
				write_log(sptr, "Created group %1%", args[1]);
			} else {
				write_log(sptr, "Updated group %1%", args[1]);
			}
			for (auto&& field : root.children()) {
				std::string fname = field.name();
				int n = db.exec_scalar("SELECT count(*) FROM pragma_table_info('groups') WHERE [name]=?", fname).get_int(0);
				if (n > 0 && !same_text(fname, "name")) {
					sp.exec_dml(format_str("UPDATE [groups] SET [%1%]=? WHERE [name]=?", fname), field.child_value(), args[1]);
				}
			}
			sp.release();
			sptr->reply(RC_POST_OK, "Request completed");
		} else {
			sptr->reply(RC_POST_REJECTED, "GROUP requires XML data");
		}
		return 1;
	}
	return 0;
}

int nntpd::server::ctl_rmgroup(nntpd::client_ptr sptr, std::string const& command, post_info& data)
{
	if (same_text(command, "RMGROUP") && (sptr->flags & SF_ADMIN)) {
		auto group = get_group(data.headers.at("Newsgroups"));
		if (group && !strmatch(group->name, "ctl.*") && 0 == group->count) {
			db.exec_dml("DELETE FROM [groups] WHERE [name]=?", group->name);
			write_log(sptr, "Deleted group %1%", group->name);
		}
		sptr->reply(RC_POST_OK, "Request completed");
		return 1;
	}
	return 0;
}

int nntpd::server::ctl_cancel(nntpd::client_ptr sptr, std::string const& command, post_info& data)
{
	std::vector<std::string> args;
	if (strscan(command, R"(^CANCEL\s+(\S+)$)", args) && (sptr->flags & SF_AUTH)) {
		auto info = get_article(args[1]);
		if (!info || strmatch(info->group, "ctl.*")) {
			sptr->reply(RC_POST_REJECTED, "Request denied: invalid article");
		} else if (!same_text(data.headers.at("From"), info->from) && !allow_group(sptr, info->group, GROUP_MODERATE)) {
			sptr->reply(RC_POST_REJECTED, "Request denied: not authorized");
		} else {
			sqlite3xx::savepoint sp{ db, "delete_article" };
			sp.exec_dml("DELETE FROM [articles] WHERE [message_id]=?1", info->message_id);
			if (data.headers.count("Supersedes")) {
				data.headers.erase("Supersedes");
				data.headers.erase("Control");
				auto super_id = store_article(data);
				write_log(sptr, "Deleted article %1% in %2%, superseded by %3%", info->message_id, info->group, super_id);
			} else {
				write_log(sptr, "Deleted article %1% in %2%", info->message_id, info->group);
			}
			sp.release();
			sptr->reply(RC_POST_OK, "Request completed");
		}
		return 1;
	}
	return 0;
}
