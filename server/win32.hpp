﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

#if BOOST_OS_WINDOWS
# pragma warning( push )
# pragma warning( disable: 4302 4838 4091 )
# pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' ""version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
# define _WTL_USE_CSTRING
# include <atlbase.h>
# include <wtl/atlapp.h>
# if defined(WIN32_LEAN_AND_MEAN)
#  include <shellapi.h>
# endif
# include <atlwin.h>
# include <atlcom.h>
# include <atlhost.h>
# include <atlctl.h>
# include <wtl/atlframe.h>
# include <wtl/atlctrls.h>
# include <wtl/atldlgs.h>
# include <wtl/atlmisc.h>
# include <atlconv.h>
# include <wtl/atlctrlw.h>
# include <wtl/atlctrlx.h>
# include <wtl/atlddx.h>
# include "resource.h"
# pragma warning( pop )

struct log_column {
	LPCWSTR label;
	int width;
	int align;
};

class CMainForm : public CDialogImpl<CMainForm> {
private:
	nntpd::server& server_;
	CListViewCtrl logView_;
	CStatic clockTime_;
	int64_t lastLogId_;
	int viewLimit_;
	std::unique_ptr<asio::deadline_timer> timer1_;

	void timerCycle(boost::system::error_code const& e);
	void addLogView(sqlite3xx::statement&&);

public:
	enum { IDD = IDD_MAIN };

	CMainForm(nntpd::server& srv)
		: CDialogImpl<CMainForm>()
		, server_{ srv }
	{}

	BEGIN_MSG_MAP(CMainForm)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_SHOWWINDOW, OnOpenDialog)
		MESSAGE_HANDLER(WM_CLOSE, OnCloseQuery)
	END_MSG_MAP()

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnOpenDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCloseQuery(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};

#endif
