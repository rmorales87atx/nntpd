﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

namespace nntpd {
	ordered_map_text<std::string, header_info> const server::header_fields = {
		{ "message_id", { "Message-ID", HF_SKIP_TBL } },
		{ "references", { "References", HF_ACCEPT | HF_SKIP_TBL } },
		{ "date", { "Date", HF_SKIP_TBL } },
		{ "expire", { "Expires", HF_SKIP_TBL } },
		{ "from", { "From", HF_ACCEPT | HF_REQUIRED | HF_CTL_OPTIONAL | HF_SKIP_TBL } },
		{ "subject", { "Subject", HF_ACCEPT | HF_REQUIRED | HF_CTL_OPTIONAL | HF_SKIP_TBL } },
		{ "content_type", { "Content-Type", HF_ACCEPT | HF_REQUIRED | HF_CTL_OPTIONAL | HF_SKIP_TBL } },
		{ "encoding", { "Content-Transfer-Encoding", HF_ACCEPT | HF_CTL_OPTIONAL | HF_SKIP_TBL } },
		{ "mime_version", { "Mime-Version", HF_ACCEPT } },
		{ "comments", { "Comments", HF_ACCEPT } },
		{ "keywords", { "Keywords", HF_ACCEPT } },
		{ "reply_to", { "Reply-To", HF_ACCEPT } },
		{ "sender", { "Sender", HF_REJECT } },
		{ "approved", { "Approved", HF_REJECT } },
		{ "archive", { "Archive", HF_ACCEPT } },
		{ "distribution", { "Distribution", HF_ACCEPT } },
		{ "followup_to", { "Followup-To", HF_ACCEPT | HF_GROUP } },
		{ "organization", { "Organization", HF_ACCEPT } },
		{ "summary", { "Summary", HF_ACCEPT } },
		{ "supersedes", { "Supersedes", HF_ACCEPT | HF_MSGID } },
		{ "user_agent", { "User-Agent", HF_ACCEPT } },
		{ "xref", { "Xref", HF_REJECT } },
	};
}

static
nntpd::group_info make_group_info(sqlite3xx::statement& stmt)
{
	nntpd::group_info out;
	out.name = stmt("name").get_text();
	out.title = stmt("title").get_text();
	out.description = stmt("description").get_text();
	out.auto_expire = stmt("auto_expire").get_uint(0);
	out.restrict_read = (0 != stmt("restrict_read").get_int(0));
	out.restrict_post = (0 != stmt("restrict_post").get_int(0));
	out.require_tls = (0 != stmt("require_tls").get_int(0));
	out.body_limit = stmt("body_limit").get_uint(0);
	out.subject_limit = stmt("subject_limit").get_uint(0);
	out.count = stmt("count").get_uint(0);
	out.high_id = stmt("high_id").get_uint(0);
	out.low_id = stmt("low_id").get_uint(0);
	return out;
}

std::optional<nntpd::group_info> nntpd::server::get_group(std::string const& group_name)
{
	auto info = db.exec_query("SELECT * FROM [group_list] WHERE [name] = ?", group_name);
	if (!info.eof()) {
		return make_group_info(info);
	}
	return std::nullopt;
}

std::vector<nntpd::group_info> nntpd::server::get_groups()
{
	std::vector<nntpd::group_info> groups;
	auto sql = db.exec_query("SELECT * FROM [group_list] ORDER BY [name]");
	for (; !sql.eof(); sql.step()) {
		groups.emplace_back(make_group_info(sql));
	}
	return groups;
}

static
nntpd::article_info make_article_info(sqlite3xx::statement const& src)
{
	assert(!src.eof());
	nntpd::article_info out;
	out.group = src("group").get_text();
	out.article_id = src("article_id").get_int64(0);
	out.message_id = src("message_id").get_text();
	out.references = src("references").get_text();
	out.subject = src("subject").get_text();
	out.from = src("from").get_text();
	out.content_type = src("content_type").get_text();
	out.encoding = src("encoding").get_text();
	out.date = src("date").datetime_to_unix();
	out.expire = src("expire").datetime_to_unix();
	out.bytes = src("bytes").get_uint(0);
	out.lines = src("lines").get_uint(0);
	for (auto&& [db_name, info] : nntpd::server::header_fields) {
		if (!src.is_field_null(db_name) && !(info.flags & nntpd::HF_SKIP_TBL)) {
			out.headers[db_name] = src(db_name).get_text();
		}
	}
	return out;
}

static
inline std::optional<nntpd::article_info> get_article_info(sqlite3xx::statement const& src)
{
	if (!src.eof()) {
		return std::make_optional(make_article_info(src));
	}
	return std::nullopt;
}

std::vector<nntpd::article_info> nntpd::server::get_articles(std::string const& group)
{
	std::vector<article_info> result;
	auto sql = db.exec_query("SELECT [rowid], * FROM [articles] WHERE [group]=?", group);
	for (; !sql.eof(); sql.step()) {
		result.emplace_back(make_article_info(sql));
	}
	return result;
}

std::optional<nntpd::article_info> nntpd::server::last_article(std::string const& group, int64_t current_id)
{
	auto stmt = db.compile("SELECT [rowid], * FROM [articles] WHERE [group]=?1 AND [article_id] < ?2 LIMIT 1");
	stmt.bind(1, group);
	stmt.bind(2, current_id);
	stmt.step();
	return get_article_info(stmt);
}

std::optional<nntpd::article_info> nntpd::server::next_article(std::string const& group, int64_t current_id)
{
	auto stmt = db.compile("SELECT [rowid], * FROM [articles] WHERE [group]=? AND [article_id] > ? LIMIT 1");
	stmt.bind(1, group);
	stmt.bind(2, current_id);
	stmt.step();
	return get_article_info(stmt);
}

int64_t nntpd::server::count_articles(std::string const& group)
{
	return db.exec_scalar("SELECT [count] FROM [group_list] WHERE [name]=?", group).get_int64(0);
}

bool nntpd::server::allow_group(client_ptr usr, std::string const& group, allow_group_action action)
{
	auto grp = get_group(group);
	// non-existing group should be handled elsewhere
	if (!grp) {
		return true;
	}
	// check if the group requires a secure connection
	if (grp->require_tls && !(usr->flags & SF_SECURE)) {
		return false;
	}
	// determine current group access
	auto access = 0;
	if ((usr->flags & SF_AUTH)) access = get_group_access(usr->account, grp->name);
	// control groups ("ctl.*") requires client to be server admin
	// or moderator of at least 1 group
	if (strmatch(group, "ctl.*")) {
		if (!(usr->flags & SF_AUTH)) {
			return false;
		}
		int mod_count = db.exec_scalar("SELECT count(*) FROM [access] WHERE [moderator]<>0 AND [account_id]=?", usr->account).get_int(0);
		return ((usr->flags & SF_ADMIN) || mod_count > 0) && (action == GROUP_READ || action == GROUP_POST);
	}
	// allow read if not restricted or client is admin
	if (action == GROUP_READ) {
		return (access & GA_READ);
	}
	// allow post if not restricted and client is authenticated
	if (action == GROUP_POST) {
		return (!grp->restrict_post || (access & GA_POST)) && (usr->flags & SF_AUTH);
	}
	// denied
	return false;
}

unsigned int nntpd::server::get_group_access(std::string const& account, std::string const& group)
{
	auto access = db.exec_query("SELECT [read], [post], [moderator] FROM [access] WHERE [group]=? AND [account_id]=?",
		group, account);
	unsigned int result = 0;
	if (!access.eof()) {
		if (0 != access("read").get_int(0)) result |= GA_READ;
		if (0 != access("post").get_int(0)) result |= GA_POST;
		if (0 != access("moderator").get_int(0)) result |= GA_MODERATOR;
	}
	return result;
}

std::optional<nntpd::article_info> nntpd::server::get_article(std::string const& msg_id)
{
	return get_article_info(db.exec_query("SELECT * FROM [articles] WHERE [message_id]=?", msg_id));
}

std::vector<nntpd::article_info> nntpd::server::group_articles(std::string const& group)
{
	std::vector<article_info> result;
	auto sql = db.exec_query("SELECT * FROM [articles] WHERE [group]=? ORDER BY [group], [article_id]", group);
	for (; !sql.eof(); sql.step()) {
		result.emplace_back(make_article_info(sql));
	}
	return result;
}

std::string nntpd::server::get_article_body(std::string const& msg_id)
{
	return db.exec_scalar("SELECT [body] FROM [articles] WHERE [message_id]=?", msg_id).get_text();
}

std::vector<nntpd::article_info> nntpd::server::get_articles(nntpd::client_ptr src, std::string const& param, std::optional<response_code> rc)
{
	std::string find;
	if (!param.empty()) {
		find = param;
	} else if (src->cur_article_id) {
		find = std::to_string(*src->cur_article_id);
	}
	// search either by message-id or article number
	std::vector<std::string> parts;
	if (strscan(find, R"(^<[\w\-\@\.]+>$)")) {
		if (auto info = get_article(find)) {
			// check for authorization
			if (!allow_group(src, info->group, GROUP_READ)) {
				src->reply(RC_MUST_AUTH);
			} else if (rc) {
				int64_t number = 0;
				if ((src->flags & SF_NEWSGROUP) && same_text(info->group, src->newsgroup)) {
					number = info->article_id;
				}
				src->reply(*rc, number, info->message_id);
			}
			return { *info };
		} else {
			src->reply(RC_MSGID_NOT_FOUND);
		}
		return {};
	} else if (!(src->flags & SF_NEWSGROUP)) {
		src->reply(RC_NO_SELECTED_GROUP);
		return {};
	} else if (find.empty()) {
		src->reply(RC_NO_SELECTED_ARTICLE);
		return {};
	}
	sqlite3xx::statement sql{ db };
	if (strscan(find, R"(^\s*(\d+)\s*\-\s*(\d*)\s*$)", parts)) {
		// search by range
		std::string query, begin, end;
		begin = parts[1];
		if (parts[2].empty()) {
			query = "SELECT * FROM [articles] WHERE [group]=? AND [article_id] >= ?";
		} else {
			end = parts[2];
			query = "SELECT * FROM [articles] WHERE [group]=? AND ([article_id] BETWEEN ? AND ?)";
		}
		query.append(" ORDER BY [group], [article_id]");
		sql = db.exec_query(query, src->newsgroup, begin, end);
	} else {
		// search by specific article number in the current group
		sql = db.exec_query("SELECT * FROM [articles] WHERE [group]=? AND [article_id]=?", src->newsgroup, find);
	}
	std::vector<article_info> result;
	for (; !sql.eof(); sql.step()) {
		auto const& info = result.emplace_back(make_article_info(sql));
		if (result.size() == 1) {
			src->cur_article_id = info.article_id;
			if (rc) src->reply(*rc, info.article_id, info.message_id);
		}
	}
	if (result.empty()) {
		src->reply(RC_ARTICLE_NOT_FOUND);
	}
	return result;
}

static
void print_header(std::stringstream& out, std::string const& name, std::string const& value)
{
	out << name << ": ";
	int n = 0;
	for (auto&& line : nntpd::get_crlf_lines(value)) {
		if (n++ > 0) out << " ";
		out << line << "\r\n";
	}
}

void nntpd::server::get_headers(article_info const& src, std::stringstream& out)
{
	char path[256] = { 0 };
	sscanf(src.message_id.c_str(), "<%*[^@]@%255[^>]>", path);
	out << "Path: " << path << "!not-for-mail\r\n";
	out << "Message-ID: " << src.message_id << "\r\n";
	if (!src.references.empty()) {
		out << "References: " << src.references << "\r\n";
	}
	if (src.expire != 0) {
		out << "Expires: " << timestamp(RFC2822_DATE_FORMAT, src.expire) << "\r\n";
	}
	out << "Date: " << timestamp(RFC2822_DATE_FORMAT, src.date) << "\r\n";
	print_header(out, "From", src.from);
	print_header(out, "Subject", src.subject);
	for (auto&& [db_name, info] : header_fields) {
		if ((info.flags & HF_SKIP_TBL)) continue;
		auto i = src.headers.find(db_name);
		if (i != std::end(src.headers)) {
			print_header(out, info.rfc_name, i->second);
		}
	}
	print_header(out, "Newsgroups", src.group);
	print_header(out, "Content-Type", src.content_type);
	print_header(out, "Content-Transfer-Encoding", src.encoding);
}

std::string nntpd::server::generate_msgid()
{
	static auto gen = boost::uuids::name_generator{ boost::uuids::ns::dns() };
	std::string value{ config->site_name + "/" + std::to_string(timestamp()) };
	auto uuid = gen(value);
	return format_str("<%1%@%2%>", boost::uuids::to_string(uuid), config->site_name);
}

void nntpd::server::post_article(nntpd::client_ptr sptr, size_t nlines, std::string_view data)
{
	post_info res;
	try {
		res.lines = nlines;
		parse_article(data, res, sptr);
		if (res.headers.count("Control")) {
			process_control(sptr, res);
		} else if (strmatch(res.group_name, "ctl.*")) {
			throw article_rejected{ "regular articles not allowed" };
		} else {
			auto msg_id = store_article(res);
			// update sptr's current newsgroup/article
			// and send final response
			auto data = get_article(msg_id);
			sptr->cur_article_id = data->article_id;
			sptr->newsgroup = data->group;
			sptr->flags |= SF_NEWSGROUP;
			write_log(sptr, "Article %1% posted to %2%", *sptr->cur_article_id, sptr->newsgroup);
			sptr->reply(RC_POST_OK, "Article accepted");
		}
	} catch (article_rejected const& err) {
		std::string const reason{ err.what() };
		write_log(sptr, "Article rejected: %1%", reason);
		sptr->reply(RC_POST_REJECTED, "Article rejected: " + reason);
	} catch (sqlite3xx::error const& err) {
		auto ref = write_log(sptr, "Database error posting article: %1%", err.what());
		sptr->reply(RC_POST_REJECTED, "Database error (ref #%1%)", ref);
	}
}

std::string nntpd::server::store_article(post_info const& data)
{
	auto msg_id = generate_msgid();
	sqlite3xx::savepoint sp{ db, "post_article" };
	// create main article record
	auto stmt = db.compile("INSERT INTO [articles]([group], [article_id], [message_id], [references], [from], [subject], [date], [expire], [body], [lines], [bytes], [content_type], [encoding])"
		" VALUES(@group, (SELECT 1 + coalesce(MAX([article_id]),0) FROM [articles] WHERE [articles].[group] = @group), @msg_id, @ref, @from, @subject, datetime('now'), @expire, @body, @lines, @bytes, @content_type, @encoding)");
	stmt.bind("@group", data.group_name);
	stmt.bind("@msg_id", msg_id);
	stmt.bind("@ref", data.references);
	stmt.bind("@from", data.headers.at("From"));
	stmt.bind("@subject", data.headers.at("Subject"));
	stmt.bind("@body", data.body);
	stmt.bind("@lines", data.lines);
	stmt.bind("@bytes", data.bytes);
	stmt.bind("@content_type", data.headers.at("Content-Type"));
	stmt.bind("@encoding", data.headers.at("Content-Transfer-Encoding"));
	auto exp_add = db.exec_scalar("SELECT [auto_expire] FROM [groups] WHERE [name]=?", data.group_name).get_uint(0);
	if (exp_add > 0) {
		stmt.bind("@expire", nntpd::timestamp("%Y-%m-%d %H:%M:%S", nntpd::timestamp() + (60 * exp_add)));
	} else {
		stmt.bind_null("@expire");
	}
	stmt.step();
	stmt.reset();
	// update optional headers if present
	for (auto&& [db_name, info] : header_fields) {
		if ((info.flags & HF_SKIP_TBL)) continue;
		if (!(info.flags & HF_ACCEPT)) continue;
		auto i = data.headers.find(info.rfc_name);
		if (i != std::end(data.headers)) {
			sp.exec_dml("UPDATE [articles] SET [" + db_name + "]=? WHERE [message_id]=?", i->second, msg_id);
		}
	}
	sp.release();
	return msg_id;
}

int64_t nntpd::server::move_article(std::string const& msg_id, std::string const& newgroup)
{
	int64_t num = 0;
	sqlite3xx::savepoint sp{ db, "MOVE_ARTICLE" };
	auto dest = get_group(newgroup);
	auto expire = dest->auto_expire;
	auto start = 1 + dest->high_id;
	auto stmt = db.compile("UPDATE [articles] SET [group]=@newgroup, [article_id]=@new_id, [xref]=(@old_group||':'||@old_id), [expire]=NULL WHERE [message_id]=@msg_id");
	for (auto res = sp.exec_query("SELECT * FROM [articles] WHERE ? IN ([message_id], [references])", msg_id); !res.eof(); res.step(), num++) {
		stmt.bind("@newgroup", dest->name);
		stmt.bind("@new_id", start++);
		stmt.bind("@msg_id", res("message_id"));
		stmt.bind("@old_group", res("group"));
		stmt.bind("@old_id", res("article_id"));
		stmt.step();
		stmt.reset();
	}
	if (expire > 0) {
		int64_t tmpt = timestamp() + (int64_t)(60 * expire);
		sp.exec_dml("UPDATE [articles] SET [expire]=?2 WHERE [message_id]=?1", msg_id, timestamp("%Y-%m-%d %H:%M:%S", tmpt));
	}
	sp.release();
	return num;
}
