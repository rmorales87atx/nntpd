﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

char const* init_sql = R"(/* Database properties */
PRAGMA foreign_keys = 'on';
PRAGMA auto_vacuum = 0;
PRAGMA encoding = 'UTF-8';
PRAGMA page_size = 4096;
PRAGMA journal_mode = 'WAL';

/* Table structure [accounts] */
CREATE TABLE IF NOT EXISTS [accounts](
  [email] TEXT PRIMARY KEY NOT NULL COLLATE NOCASE, 
  [display_name] TEXT, 
  [pw_hash] TEXT, 
  [active] BOOL NOT NULL DEFAULT 1, 
  [admin] BOOL NOT NULL DEFAULT 0, 
  [crosspost] INTEGER);
CREATE UNIQUE INDEX IF NOT EXISTS [accounts_id] ON [accounts]([email] COLLATE [NOCASE]);

/* Table structure [groups] */
CREATE TABLE IF NOT EXISTS [groups](
  [name] TEXT NOT NULL UNIQUE COLLATE NOCASE, 
  [title] TEXT NOT NULL UNIQUE, 
  [description] TEXT, 
  [created] DATETEXT NOT NULL DEFAULT (datetime('now')), 
  [auto_expire] INTEGER NOT NULL DEFAULT 0, 
  [restrict_read] BOOL NOT NULL DEFAULT 0, 
  [restrict_post] BOOL NOT NULL DEFAULT 0, 
  [require_tls] BOOL NOT NULL DEFAULT 0, 
  [body_limit] INTEGER NOT NULL DEFAULT 8192, 
  [subject_limit] INTEGER NOT NULL DEFAULT 100, 
  PRIMARY KEY([name] COLLATE [NOCASE]));
CREATE UNIQUE INDEX IF NOT EXISTS [group_name] ON [groups]([name]);
CREATE UNIQUE INDEX IF NOT EXISTS [group_title] ON [groups]([title]);

/* Table structure [access] */
CREATE TABLE IF NOT EXISTS [access](
  [group] TEXT NOT NULL COLLATE NOCASE REFERENCES [groups]([name]) ON DELETE CASCADE ON UPDATE CASCADE, 
  [account_id] TEXT NOT NULL COLLATE NOCASE REFERENCES [accounts]([email]) ON DELETE CASCADE ON UPDATE CASCADE, 
  [read] BOOL NOT NULL DEFAULT 1, 
  [post] BOOL NOT NULL DEFAULT 1, 
  [moderator] BOOL NOT NULL DEFAULT 0, 
  PRIMARY KEY([group], [account_id]));
CREATE INDEX IF NOT EXISTS [access_id]
ON [access](
  [group] COLLATE [NOCASE], 
  [account_id] COLLATE [NOCASE]);

/* Table structure [articles] */
CREATE TABLE IF NOT EXISTS [articles](
  [group] TEXT NOT NULL REFERENCES [groups]([name]) ON DELETE RESTRICT ON UPDATE CASCADE, 
  [article_id] INTEGER NOT NULL, 
  [message_id] TEXT PRIMARY KEY NOT NULL, 
  [references] TEXT REFERENCES [articles]([message_id]) ON DELETE CASCADE ON UPDATE CASCADE, 
  [date] DATETIME NOT NULL DEFAULT (DATETIME ('now')), 
  [expire] DATETIME, 
  [from] TEXT NOT NULL, 
  [subject] TEXT NOT NULL, 
  [lines] INTEGER NOT NULL, 
  [bytes] INTEGER NOT NULL DEFAULT 0, 
  [content_type] TEXT NOT NULL DEFAULT 'text/plain; charset=utf-8', 
  [encoding] TEXT NOT NULL DEFAULT '7bit', 
  [body] TEXT, 
  [comments] TEXT, 
  [keywords] TEXT, 
  [reply_to] TEXT, 
  [sender] TEXT, 
  [approved] TEXT, 
  [archive] TEXT, 
  [distribution] TEXT, 
  [followup_to] TEXT, 
  [organization] TEXT, 
  [summary] TEXT, 
  [supersedes] TEXT REFERENCES [articles]([message_id]) ON DELETE SET NULL ON UPDATE CASCADE, 
  [user_agent] TEXT, 
  [xref] TEXT, 
  [mime_version] TEXT DEFAULT '1.0');
CREATE UNIQUE INDEX IF NOT EXISTS [articles_msgid] ON [articles]([message_id]);
CREATE UNIQUE INDEX IF NOT EXISTS [articles_id]
ON [articles](
  [group], 
  [article_id]);

/* Table structure [log] */
CREATE TABLE IF NOT EXISTS [log](
  [id] INTEGER PRIMARY KEY NOT NULL, 
  [timestamp] DATETIME NOT NULL DEFAULT (DATETIME ('now')), 
  [source] TEXT, 
  [message] TEXT);

/* View structure [group_list] */
CREATE VIEW IF NOT EXISTS [group_list]
AS
SELECT 
       [groups].*, 
       (SELECT COUNT (*)
       FROM   [articles]
       WHERE  [articles].[group] = [groups].[name]) AS [count], 
       COALESCE ((SELECT MAX ([article_id])
       FROM   [articles]
       WHERE  [articles].[group] = [groups].[name]
       GROUP  BY [group]), 0) AS [high_id], 
       COALESCE ((SELECT MIN ([article_id])
       FROM   [articles]
       WHERE  [articles].[group] = [groups].[name]
       GROUP  BY [group]), 0) AS [low_id]
FROM   [groups];
)";
