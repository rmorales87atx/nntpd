﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using namespace std::placeholders;

#if BOOST_OS_WINDOWS
# include "win32.hpp"

LRESULT CMainForm::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	static log_column cols[] = { 
		{ L"ID", 65, LVCFMT_RIGHT },
		{ L"Date/Time", 115, LVCFMT_LEFT },
		{ L"Source", 130, LVCFMT_LEFT },
		{ L"Message", 300, LVCFMT_LEFT },
	};
	logView_.Attach(GetDlgItem(IDC_LIST1));
	ListView_SetExtendedListViewStyle(logView_, LVS_EX_FULLROWSELECT);
	int cx = 0;
	for (auto col : cols) {
		logView_.InsertColumn(cx++, col.label, col.align, col.width);
	}
	viewLimit_ = 1000; // default
	lastLogId_ = 0;
	clockTime_.Attach(GetDlgItem(IDC_TIME));
	return 0;
}

LRESULT CMainForm::OnOpenDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	viewLimit_ = server_.config->win32_view_limit;
	if (server_.config->win32_view_init > 0) {
		addLogView(server_.db.exec_query("SELECT * FROM (SELECT * FROM [log] ORDER BY [id] DESC LIMIT ?) ORDER BY [id] ASC",
			server_.config->win32_view_init));
	} else {
		lastLogId_ = server_.db.exec_scalar("SELECT max([id]) FROM [log]").get_int64(0);
	}
	timer1_.reset(new asio::deadline_timer{ server_.ios, boost::posix_time::seconds(1) });
	timer1_->async_wait(std::bind(&CMainForm::timerCycle, this, _1));
	return 0;
}

LRESULT CMainForm::OnCloseQuery(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	timer1_.reset();
	server_.close();
	PostQuitMessage(0);
	return 0;
}

void CMainForm::timerCycle(boost::system::error_code const& err)
{
	if (!err) {
		auto val = nntpd::str_widen(nntpd::timestamp("%Y-%m-%d %H:%M:%S"));
		clockTime_.SetWindowTextW(val.c_str());
		addLogView(server_.db.exec_query("SELECT * FROM [log] WHERE [id]>?", lastLogId_));
		timer1_->expires_from_now(boost::posix_time::seconds(1));
		timer1_->async_wait(std::bind(&CMainForm::timerCycle, this, _1));
	}
}

void CMainForm::addLogView(sqlite3xx::statement&& query)
{
	int nIndex = -1;
	for (; !query.eof(); query.step()) {
		if (logView_.GetItemCount() == viewLimit_) {
			logView_.DeleteItem(0);
		}
		nIndex = logView_.AddItem(logView_.GetItemCount(), 0, L"");
		for (int ncol = 0; ncol < query.field_count(); ncol++) {
			if (auto v = query.field_value(ncol); !v.is_null()) {
				logView_.SetItemText(nIndex, ncol, v.wc_str());
			}
		}
		lastLogId_ = *query("id").get_int64();
	}
	if (nIndex >= 0) logView_.EnsureVisible(nIndex, FALSE);
}

#endif
