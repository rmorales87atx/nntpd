﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

#if BOOST_OS_WINDOWS
# define GMTIME(TM,T) ::gmtime_s(TM,T)
# define LOTIME(TM,T) ::localtime_s(TM,T)
#else
# define GMTIME(TM,T) ::gmtime_r(T,TM)
# define LOTIME(TM,T) ::localtime_r(T,TM)
#endif

using boost::locale::collator;
using boost::locale::collator_base;
using boost::locale::normalize;
using boost::locale::fold_case;
using boost::locale::to_upper;

int nntpd::compare_nfc(std::string_view left_, std::string_view right_)
{
	auto left{ normalize_nfc(left_) };
	auto right{ normalize_nfc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

int nntpd::compare_nfkc(std::string_view left_, std::string_view right_)
{
	auto left{ normalize_nfkc(left_) };
	auto right{ normalize_nfkc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

int nntpd::compare_text(std::string_view left_, std::string_view right_)
{
	auto left{ fold_case_nfkc(left_) };
	auto right{ fold_case_nfkc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

bool nntpd::same_text(std::string_view left_, std::string_view right_)
{
	return 0 == compare_text(left_, right_);
}

std::string nntpd::fold_case_nfkc(std::string_view str)
{
	return normalize(fold_case(std::string(str)), boost::locale::norm_nfkc);
}

std::string nntpd::normalize_nfkc(std::string_view str)
{
	return normalize(std::string(str), boost::locale::norm_nfkc);
}

std::string nntpd::fold_case_nfc(std::string_view str)
{
	return normalize(fold_case(std::string(str)), boost::locale::norm_nfc);
}

std::string nntpd::normalize_nfc(std::string_view str)
{
	return normalize(std::string(str), boost::locale::norm_nfc);
}

std::string nntpd::upper_case_nfc(std::string_view str)
{
	return normalize(to_upper(std::string(str)), boost::locale::norm_nfc);
}

std::string nntpd::upper_case_nfkc(std::string_view str)
{
	return normalize(to_upper(std::string(str)), boost::locale::norm_nfkc);
}

time_t nntpd::timestamp()
{
	time_t const my_time = time(nullptr);
	struct tm curtime;
	GMTIME(&curtime, &my_time);
	return mktime(&curtime);
}

std::string nntpd::timestamp(char const* format, std::optional<time_t> timepoint)
{
	time_t my_time{ timepoint.value_or(time(nullptr)) };
	struct tm curtime;

	if (strmatch(format, ".*%z.*")) {
		LOTIME(&curtime, &my_time);
	} else {
		GMTIME(&curtime, &my_time);
	}

	char buf[100];
	strftime(buf, sizeof(buf), format, &curtime);

	return buf;
}

bool nntpd::strscan(std::string const& input, std::string const& pattern)
{
	auto pat = str_widen(pattern);
	auto inp = str_widen(input);
	boost::wregex expr{ pat, boost::regex::perl | boost::regex::icase };
	return boost::regex_match(inp, expr);
}

bool nntpd::strscan(std::string const& input, std::string const& pattern, std::vector<std::string>& results)
{
	auto pat = str_widen(pattern);
	auto inp = str_widen(input);
	boost::wregex expr{ pat, boost::regex::perl | boost::regex::icase };
	boost::wsmatch res;
	if (boost::regex_match(inp, res, expr)) {
		for (unsigned int i = 0; i < res.size(); i++) {
			results.push_back(str_narrow(res[i].first, res[i].second));
		}
		return true;
	}
	return false;
}

bool nntpd::strmatch(std::string_view expr, std::string_view match, std::optional<char32_t> any_char, std::optional<char32_t> one_char)
{
	// adapted from https://www.geeksforgeeks.org/wildcard-pattern-matching/
	using std::vector;
	std::string str, pattern;
	utf8::replace_invalid(fold_case_nfkc(expr), std::back_inserter(str));
	utf8::replace_invalid(fold_case_nfkc(match), std::back_inserter(pattern));
	size_t const n = utf8::distance(std::begin(str), std::end(str));
	size_t const m = utf8::distance(std::begin(pattern), std::end(pattern));
	size_t i, j;

	if (m == 0) {
		return (n == 0);
	}

	vector<vector<bool>> lookup;

	lookup.resize(n + 1);
	for (size_t i = 0; i <= n; i++) {
		lookup[i].resize(m + 1);
	}

	lookup[0][0] = true;

	if (!any_char) any_char = '*';
	if (!one_char) one_char = '?';

	j = 1;
	for (auto p = utf8::begin(pattern); p != utf8::end(pattern); j++, p++) {
		if (*p == *any_char) {
			lookup[0][j] = lookup[0][j - 1];
		}
	}

	i = 1;
	for (auto s = utf8::begin(str); s != utf8::end(str); i++, s++) {
		j = 1;
		for (auto p = utf8::begin(pattern); p != utf8::end(pattern); j++, p++) {
			if (*p == *any_char) {
				lookup[i][j] = lookup[i][j - 1] || lookup[i - 1][j];
			} else if (*p == *one_char || *s == *p) {
				lookup[i][j] = lookup[i - 1][j - 1];
			} else {
				lookup[i][j] = false;
			}
		}
	}

	return lookup[n][m];
}
