﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using boost::system::error_code;
using namespace std::placeholders;

extern char const* init_sql; // db_sql.cpp

#if BOOST_OS_WINDOWS
# include <wincrypt.h>

static void add_windows_root_certs(boost::asio::ssl::context& ctx)
{
	HCERTSTORE hStore = CertOpenSystemStoreW(0, L"ROOT");
	if (hStore) {
		X509_STORE* store = X509_STORE_new();
		PCCERT_CONTEXT pContext = nullptr;
		while ((pContext = CertEnumCertificatesInStore(hStore, pContext))) {
			X509* x509 = d2i_X509(nullptr,
				(unsigned char const**)&pContext->pbCertEncoded,
				pContext->cbCertEncoded);
			if (x509) {
				X509_STORE_add_cert(store, x509);
				X509_free(x509);
			}
		}
		CertFreeCertificateContext(pContext);
		CertCloseStore(hStore, 0);
		SSL_CTX_set_cert_store(ctx.native_handle(), store);
	}
}

#endif

nntpd::server::server()
	: signals_{ ios }
	, ssl_context{ asio::ssl::context::tls }
{
#if !BOOST_OS_WINDOWS
	static auto lbm = boost::locale::localization_backend_manager::global();
	lbm.select("icu");
	boost::locale::localization_backend_manager::global(lbm);
#endif
	static boost::locale::generator g;
	std::locale::global(g(""));
	sqlite3xx::initialize();
	for (int sig : { SIGABRT, SIGFPE, SIGILL, SIGINT, SIGSEGV, SIGTERM }) {
		signals_.add(sig);
	}
#if defined(SIGQUIT)
	signals_.add(SIGQUIT);
#endif
#if defined(SIGBREAK)
	signals_.add(SIGBREAK);
#endif
#if defined(SIGABRT_COMPAT)
	signals_.add(SIGABRT_COMPAT);
#endif
	signals_.async_wait(std::bind(&server::handle_signal, this, _1, _2));
	ssl_context.set_options(asio::ssl::context::default_workarounds
		| asio::ssl::context::no_sslv2
		| asio::ssl::context::no_sslv3
		| asio::ssl::context::no_tlsv1
		| asio::ssl::context::single_dh_use);
	init_commands();
	init_ctl_handlers();
	init_config();
	load_mime_types();
}

nntpd::server::~server()
{
	db.close();
}

void nntpd::server::handle_signal(boost::system::error_code const& err, int signal)
{
	try {
		if (!err) close();
	} catch (...) {}
}

void nntpd::server::init_db_conn()
{
	db.open(config->db_file);
	db.set_error_mask(sqlite3xx::all_db_errors, true);
	db.create_collation("UTF8", std::bind(&compare_nfkc, _1, _2));
	db.create_collation("NOCASE", std::bind(&compare_text, _1, _2));
	// override glob() function for unicode input
	db.create_function("glob", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text()));
	});
	// override like() function for unicode input
	db.create_function("like", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text(), '%', '_'));
	});
	// initialize schema if needed
	db.exec_dml(init_sql);
	if (has_option(NNTP_SECURE_DELETE)) {
		db.exec_dml("PRAGMA secure_delete=ON");
	} else {
		db.exec_dml("PRAGMA secure_delete=OFF");
	}
}

void nntpd::server::init_config()
{
	if (!(config = load_config_xml(""))) {
		throw std::runtime_error{ "error loading config" };
	}
	for (auto&& [addr, port] : config->listen) {
		add_listen(addr, port, false);
	}
	if (config->options & NNTP_ENABLE_TLS) {
#if BOOST_OS_WINDOWS
		add_windows_root_certs(ssl_context);
#else
		ssl_context.set_default_verify_paths();
#endif
		ssl_context.use_private_key_file(config->tls_private_key, asio::ssl::context::pem);
		if (!config->tls_cert_file.empty()) {
			ssl_context.use_certificate_chain_file(config->tls_cert_file);
		}
		for (auto&& [addr, port] : config->tls_bind) {
			add_listen(addr, port, true);
		}
	}
	init_db_conn();
}

void nntpd::server::init_timer1()
{
	if (!timer1_) {
		timer1_.reset(new asio::deadline_timer{ ios, boost::posix_time::seconds(60) });
	} else {
		timer1_->expires_from_now(boost::posix_time::seconds(60));
	}
	timer1_->async_wait([=](error_code const& e) { if (!e) purge_db(); });
}

void nntpd::server::run()
{
	assert(config);
	purge_db();
	init_timer1();
	ios.run();
}

int64_t nntpd::server::write_log(client_ptr source, std::string const& message)
{
	try {
		auto sql = db.compile("INSERT INTO [log] ([source], [message]) VALUES(@src, @msg)");
		sql.bind_text("@src", source->endpoint_id());
		sql.bind_text("@msg", message);
		if (sql.step()) {
			return db.last_rowid();
		}
	} catch (sqlite3xx::error const&) {}
	return -1;
}

int64_t nntpd::server::write_log(std::string const& message)
{
	try {
		auto sql = db.compile("INSERT INTO [log] ([message]) VALUES(@msg)");
		sql.bind_text("@msg", message);
		if (sql.step()) {
			return db.last_rowid();
		}
	} catch (sqlite3xx::error const&) {}
	return -1;
}

void nntpd::server::close()
{
	boost::system::error_code ec; // not really used, just here to prevent exceptions
	timer1_->cancel(ec);
	timer1_.reset();
	listen_.clear();
	for (auto& [id, uptr] : clients_) {
		uptr->kill("Service going offline");
	}
	while (ios.run_for(std::chrono::milliseconds(500)) != 0)
		;
	ios.stop();
}

nntpd::client_ptr nntpd::server::new_client(socket_ptr sock)
{
	auto sptr = std::make_shared<nntpd::client>(sock, *this);
	clients_.insert_or_assign(sptr->endpoint_id(), sptr);
	return sptr;
}

void nntpd::server::remove_client(std::string const& id)
{
	clients_.erase(id);
}

bool nntpd::server::has_option(unsigned int value) const
{
	return (config->options & value) == value;
}

void nntpd::server::add_listen(std::string const& host, int port, bool secure)
{
	auto addr = asio::ip::address::from_string(host);
	listen_.emplace_back(std::make_unique<listener>(*this, tcp::endpoint{ addr, static_cast<unsigned short>(port) }, secure));
}

void nntpd::server::purge_db()
{
	try {
		int n = db.exec_dml("DELETE FROM [articles] WHERE datetime('now') > [expire]");
		if (n != 0) write_log("Article records purged: %1%", n);
	} catch (sqlite3xx::error const& err) {
		write_log("Database error while running purge: %1%", err.what());
	}
	if (timer1_) init_timer1();
}
