﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

/// Create a formatted string
template<typename... Args>
std::string format_str(std::string_view spec, Args&&... args)
{
	using namespace boost::io;
	boost::format fmt{ std::string(spec) };
	fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
	(void)(fmt % ... % std::forward<Args>(args));
	return fmt.str();
}

/// Convert a number to base36
template<typename IntegerT = unsigned int>
std::string to_base36(IntegerT val)
{
	static char const base36[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	std::string result;
	do {
		result = base36[val % 36] + result;
	} while (val /= 36);
	return result;
}

//! Join elements of a given sequence
template<typename SequenceT>
std::string join(SequenceT const& input, char const* strSep, size_t iStart = 0, size_t iStop = 0)
{
	std::string result;
	auto iter = input.begin();
	if (iStart > 0 && iStart < input.size()) {
		std::advance(iter, iStart);
	}
	auto stop = input.end();
	if (iStop > 0 && iStop < input.size()) {
		stop = input.begin();
		std::advance(stop, iStop);
	}
	if (iter != stop) {
		result += *iter;
		for (iter++; iter != stop; iter++) {
			result += strSep + *iter;
		}
	}
	return result;
}

namespace nntpd {
	/// Comparator object for use in std::map (and similar containers that expect "lhs < rhs").
	/// Strings are compared using compare_text().
	struct text_comparator_less {
		bool operator()(std::string_view lhs, std::string_view rhs) const;
	};

	/// Comparator object for use in std::set (and other containers where containers expect "lhs == rhs").
	/// Strings are compared using compare_text().
	struct text_comparator {
		bool operator()(std::string_view lhs, std::string_view rhs) const;
	};

	// tsl::ordered_map<> with case-insensitive key strings
	template<typename StringT, typename ValueT>
	using ordered_map_text = tsl::ordered_map<StringT, ValueT, std::hash<StringT>, text_comparator>;

	/// Normalize text to NFC and perform comparison.
	int compare_nfc(std::string_view left, std::string_view right);

	/// Normalize text to NFKC and perform comparison.
	int compare_nfkc(std::string_view left, std::string_view right);

	/// Case-fold and normalize text to NFC and perform comparison.
	int compare_text(std::string_view left, std::string_view right);

	/// Determine if two strings contain the same text, case-folded and normalized to NFKC.
	/// Essentially a shortcut for "0 == compare_text(left, right)"
	bool same_text(std::string_view left, std::string_view right);

	/// Case-fold and normalize text to NFC.
	std::string fold_case_nfc(std::string_view str);

	/// Case-fold and normalize text to NFKC.
	std::string fold_case_nfkc(std::string_view str);

	/// Normalize text to NFC.
	std::string normalize_nfc(std::string_view str);

	/// Normalize text to NFKC.
	std::string normalize_nfkc(std::string_view str);

	/// Upper-case and normalize text to NFC.
	std::string upper_case_nfc(std::string_view str);

	/// Upper-case and normalize text to NFKC.
	std::string upper_case_nfkc(std::string_view str);

	/// Determine current UTC time
	time_t timestamp();

	/// Construct a formatted UTC timestamp
	/// EXCEPTION: if %z is present in format, the result will be in local time
	std::string timestamp(char const* format, std::optional<time_t> timepoint = std::nullopt);

	/// Scan text against a regular expression
	bool strscan(std::string const& input, std::string const& pattern);

	/// Scan text against a regular expression
	bool strscan(std::string const& input, std::string const& pattern, std::vector<std::string>& results);

	/// Convert UTF-8 input to a wide string.
	/// Results in UTF-16 on Windows, UTF-32 on all other platforms
	std::wstring str_widen(std::string_view);

	/// Convert wide string to UTF-8
	std::string str_narrow(std::wstring_view);

	/// Convert UTF-8 input to a wide string.
	/// Results in UTF-16 on Windows, UTF-32 on all other platforms.
	template <typename iterator>
	std::wstring str_widen(iterator start, iterator end);

	/// Convert wide string to UTF-8
	/// On Windows the wide string must be UTF-16, and UTF-32 on other platforms.
	template <typename iterator>
	std::string str_narrow(iterator start, iterator end);

	/// Match an expression against the given wildcard pattern.
	/// Both the expression and match strings are case-folded and normalized.
	///
	/// @param expr String containing the expression being matched.
	/// @param pattern String containing the match pattern.
	/// @param any_char An optional value specifying the character for matching any sequence of characters.
	///                 If not present (nullopt), the default is '*'.
	/// @param one_char An optional value specifying the character for matching a single character.
	///                 If not present (nullopt), the default is '?'.
	bool strmatch(std::string_view expr, std::string_view pattern, std::optional<char32_t> any_char = std::nullopt, std::optional<char32_t> one_char = std::nullopt);

	/// Parse CRLF-delimited lines using boost::tokenizer
	inline auto get_crlf_lines(std::string const& text);

	/// Parse CRLF-delimited lines using boost::tokenizer
	template<typename T>
	inline auto get_crlf_lines(T&& begin, T&& end);
}
