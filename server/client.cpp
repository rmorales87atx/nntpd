﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "nntpd.hpp"

using boost::system::error_code;
using namespace std::placeholders;

nntpd::client::client(socket_ptr psock, nntpd::server& srv)
	: server_{ srv }
	, socket_{ psock }
	, data_{ 9216 }
	, last_action_{ 0 }
	, flags{ 0 }
{
	assert(socket_);
	error_code ec;
	auto endpt = socket_->remote_endpoint(ec);
	endpt_id_ = endpt.address().to_string(ec) + ":" + std::to_string(endpt.port());

	// setup TCP keep-alive
	constexpr unsigned int base_timeout_value = 75000; // milliseconds
#if BOOST_OS_WINDOWS
	constexpr std::int32_t w32_timeout = base_timeout_value;
	setsockopt(socket_->native_handle(), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&w32_timeout), sizeof(w32_timeout));
	setsockopt(socket_->native_handle(), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const char*>(&w32_timeout), sizeof(w32_timeout));
#else
	// assume POSIX
	struct timeval tv;
	tv.tv_sec = base_timeout_value / 1000;
	tv.tv_usec = base_timeout_value % 1000;
	setsockopt(socket_->native_handle(), SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	setsockopt(socket_->native_handle(), SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));
#endif
}

nntpd::client::~client()
{
	close();
}

std::string nntpd::client::endpoint_id() const
{
	return endpt_id_;
}

std::string nntpd::client::address() const
{
	try {
		auto address = socket_->remote_endpoint().address();
		return address.to_string();
	} catch (boost::system::system_error const&) {
		return u8"0.0.0.0";
	}
}

void nntpd::client::kill(std::string const& message)
{
	boost::system::error_code ec;
	if (socket_ && socket_->is_open()) {
		queue_.clear();
		if (!message.empty()) {
			// do a direct write to the socket; otherwise the message
			// will be lost before the socket is completely shutdown
			auto text = format_str("400 %1%\r\n", message);
			asio::write(*socket_, asio::buffer(text.c_str(), text.size()));
		}
		close();
	}
}

void nntpd::client::close()
{
	if (socket_ && socket_->is_open()) {
		queue_.clear();
		server_.write_log(shared_from_this(), "Connection closed");
		boost::system::error_code ec;
		socket_->cancel(ec);
		socket_->shutdown(tcp::socket::shutdown_both, ec);
		socket_->close(ec);
		asio::post(server_.ios, [this, self = shared_from_this()]() {
			server_.remove_client(endpoint_id());
		});
	}
}

void nntpd::client::read()
{
	if (socket_ && socket_->is_open()) {
		if (ssl_socket_) {
			asio::async_read_until(*ssl_socket_, data_, "\r\n", std::bind(&client::read_handler, this, _1, _2));
		} else {
			asio::async_read_until(*socket_, data_, "\r\n", std::bind(&client::read_handler, this, _1, _2));
		}
	}
}

void nntpd::client::read_handler(boost::system::error_code const& err, std::size_t nbytes)
{
	if (!err || (err == asio::error::not_found && data_.size() != 0)) {
		if (nbytes == 0 && err == asio::error::not_found) nbytes = data_.size();
		if (nbytes > 0) {
			auto dbuf = data_.data();
			std::string line{ buffers_begin(dbuf), buffers_begin(dbuf) + nbytes };
			boost::trim_right_if(line, boost::is_any_of("\r\n"));
			process(line);
			data_.consume(nbytes);
		}
		read();
	} else {
		close();
	}
}

void nntpd::client::write(std::string const& str)
{
	if (socket_ && socket_->is_open()) {
		// queue message for writing
		bool sending = !queue_.empty();
		queue_.push_back(str + "\r\n");
		if (!sending) {
			send_message();
		}
	}
}

void nntpd::client::write(std::stringstream const& ss, bool send_dot)
{
	if (socket_ && socket_->is_open()) {
		auto lines = ss.str();
		if (!lines.empty() && !boost::ends_with(lines, "\r\n")) lines.append("\r\n");
		if (send_dot) lines.append(".\r\n");
		if (!lines.empty()) {
			bool sending = !queue_.empty();
			queue_.push_back(lines);
			if (!sending) {
				send_message();
			}
		}
	}
}

void nntpd::client::send_message()
{
	if (socket_ && socket_->is_open() && !queue_.empty()) {
		// perform asynchronous write on the topmost message in the queue
		auto const& message{ queue_.front() };
		error_code err;
		if (ssl_socket_) {
			asio::write(*ssl_socket_, asio::buffer(message.c_str(), message.size()), err);
		} else {
			asio::write(*socket_, asio::buffer(message.c_str(), message.size()), err);
		}
		if (!err) {
			last_action_ = nntpd::timestamp();
			if (!queue_.empty()) {
				queue_.pop_front();
				send_message();
			}
		} else {
			close();
		}
	}
}

void nntpd::client::reply(response_code rc)
{
	write(generate_reply(rc));
}

void nntpd::client::send(response_code rc, std::stringstream const& sstr)
{
	write(generate_reply(rc));
	write(sstr);
}

bool nntpd::client::set_multiline(multiline_handler completion_handler)
{
	if (!(flags & SF_MULTILINE)) {
		multiline_done_ = completion_handler;
		multiline_buf_.reserve(1048576);
		multiline_count_ = 0;
		flags |= SF_MULTILINE;
		return true;
	}
	return false;
}

void nntpd::client::process(std::string const& line)
{
	try {
		static size_t const hard_limit = 1024 * 1048576; // about 1GiB - this server is not intended as a high-performance file store
		if (flags & SF_MULTILINE) {
			if (line.size() == 1 && line[0] == '.') {
				flags &= ~SF_MULTILINE;
				multiline_done_(shared_from_this(), multiline_count_, std::string_view{ multiline_buf_.data(), multiline_buf_.size() });
				multiline_buf_.clear();
				multiline_count_ = 0;
			} else {
				static std::string const crlf{ "\r\n" };
				std::copy(std::begin(line), std::end(line), std::back_inserter(multiline_buf_));
				std::copy(std::begin(crlf), std::end(crlf), std::back_inserter(multiline_buf_));
				multiline_count_++;
			}
			if (multiline_buf_.size() > hard_limit) {
				flags &= ~SF_MULTILINE;
				multiline_buf_.clear();
				kill("Too much data");
			}
		} else {
			server_.command(shared_from_this(), line);
		}
	} catch (std::exception const& err) {
		auto ref = server_.write_log(shared_from_this(), "Internal error: %1%", err.what());
		kill(format_str("Internal error (ref#%1%)", ref));
	}
}
