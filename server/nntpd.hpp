﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

#if defined(_WIN32) || defined(_MSC_VER)
# pragma warning(disable: 4267 4996)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
# undef min
# undef max
#endif

#define BOOST_BIND_NO_PLACEHOLDERS 1
#define BOOST_SPIRIT_UNICODE 1
#define BOOST_SPIRIT_X3_UNICODE 1

#include <tuple>
#include <ctime>
#include <iostream>
#include <string>
#include <string_view>
#include <memory>
#include <deque>
#include <iomanip>
#include <functional>
#include <list>
#include <forward_list>
#include <map>
#include <fstream>
#include <optional>
#include <unordered_map>
#include <boost/predef.h>
#include <boost/bind/bind.hpp>
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#if !BOOST_OS_WINDOWS
# include <boost/regex/icu.hpp>
#endif
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/functional/hash.hpp>
#include <boost/locale.hpp>

#include "date.h"
#include "utf8.h"
#include "SQLite3xx.hpp"
#include "pugixml.hpp"
#include "tsl/ordered_map.h"

namespace asio = boost::asio;
using asio::ip::tcp;

#include "misc.hpp"
#include "data.hpp"
#include "reply.hpp"
#include "client.hpp"
#include "server.hpp"

#include "nntpd.ipp" // should be last, template definitions go here
