﻿/*
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#if defined(_MSC_VER)
# pragma warning(disable: 4267 4996)
#endif

#include <boost/predef.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <optional>
#include <cstring>

#if BOOST_OS_WINDOWS
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include <Wincrypt.h>
#endif

#include "bcrypt.hpp"
#include "ow-crypt.h"

struct bcrypt_info {
	char version[9];
	int workload;
	char salt[23];
	char text[32];
};

std::optional<bcrypt_info> parse_bcrypt_text(std::string const& text)
{
	bcrypt_info data;
	memset(&data, 0, sizeof(data));
	if (sscanf(text.c_str(), "$%8[^$]$%d$%22s%31s", data.version, &data.workload, data.salt, data.text) != 0) {
		return data;
	}
	return std::nullopt;
}

std::vector<uint8_t> gen_random_bytes(size_t bytes)
{
	std::vector<uint8_t> result;
#if BOOST_OS_WINDOWS
	HCRYPTPROV hCryptProv;
	if (CryptAcquireContextW(&hCryptProv, nullptr, nullptr, PROV_RSA_AES, CRYPT_VERIFYCONTEXT)) {
		result.resize(bytes);
		if (!CryptGenRandom(hCryptProv, static_cast<DWORD>(bytes), &result[0])) {
			result.clear();
		}
		CryptReleaseContext(hCryptProv, 0);
	}
#elif BOOST_OS_UNIX
	std::ifstream fs;
	fs.open("/dev/urandom", std::ios::binary);
	if (fs.is_open()) {
		result.resize(bytes);
		fs.read(reinterpret_cast<char*>(&result[0]), bytes);
	}
#else
#error Unknown platform
#endif
	return result;
}

std::string bcrypt::generate_hash(std::string const& key, std::string const& salt)
{
	std::string result = crypt(key.c_str(), salt.c_str());
	return result;
}

std::string bcrypt::generate_hash(std::string const& key)
{
	auto salt = generate_salt();
	return generate_hash(key, salt);
}

std::string bcrypt::generate_salt(unsigned long workload, std::string const& prefix)
{
	if (workload < 4 || workload > 18) throw std::out_of_range{ "invalid bcrypt workload" };
	auto bytes = gen_random_bytes(16);
	std::string result = crypt_gensalt(prefix.c_str(), workload, reinterpret_cast<char const*>(&bytes[0]), bytes.size());
	return result;
}

int bcrypt::compare(std::string_view encoded, std::string_view plaintext)
{
	if (plaintext.empty()) return -1;
	std::string check(encoded);
	auto info = parse_bcrypt_text(check);
	if (!info) return -1;
	if (info->workload < 4 || info->workload > 18) throw std::out_of_range{ "invalid bcrypt workload" };
	std::stringstream ss;
	ss << "$" << info->version;
	ss << "$" << std::setw(2) << std::setfill('0') << info->workload;
	ss << "$" << info->salt;
	auto find = generate_hash(std::string(plaintext), ss.str());
	return check.compare(find);
}
