# Basic Internet News Service

This project implements a simple independent news server, using NNTP described in RFC 3977.

## UNIX Platforms

Tested with FreeBSD 12.1 (clang 8.0.1) and Ubuntu 20.04 (gcc 9.3.0)

- Ensure boost libraries version **1.70+** are installed using your preferred method (several libraries in use: asio, date_time, regex, spirit, locale, ...)
- In the root source directory, simply run `cmake -S . -B build` and then `cmake --build build/`.

## Visual Studio with VCPKG
### Prerequisites
- Download and install [`vcpkg`](https://github.com/microsoft/vcpkg) (don't forget to `vcpkg integrate install`)
- Install at least these boost libraries using `vcpkg`, preferably with static linking:
	- boost-algorithm
	- boost-asio
	- boost-core
	- boost-predef
	- boost-date-time
	- boost-regex
	- boost-locale
	- boost-program-options

### Generate Build Files
* Run one of these commands inside a `Developer Command Prompt`. Replace `E:/vcpkg` with your `vcpkg` location, and add/remove `-DVCPKG_TARGET_TRIPLET` as appropriate.
	* 64-bit (recommended):
		* `cmake -S . -B build64 -A x64 -DVCPKG_ROOT=E:/vcpkg -DVCPKG_TARGET_TRIPLET=x64-windows-static`
	* 32-bit
		* `cmake -S . -B build -A Win32 -DVCPKG_ROOT=E:/vcpkg -DVCPKG_TARGET_TRIPLET=x86-windows-static`
* You can open the solution files in VS to build, or simply use `cmake --build build`
